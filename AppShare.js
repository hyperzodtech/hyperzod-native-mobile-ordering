import React, {Component} from 'react';
import {Share} from 'react-native';
class AppShare extends Component {
  async openNativeShare(sharedata) {
    try {
      const result = await Share.share(sharedata.required, sharedata.optional);
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log(result.activityType);
          // shared with activity type of result.activityType
        } else {
          console.log('else');
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissedAction');
        // dismissed
      }
    } catch (error) {
      console.log(error);
    }
  }
  render() {
    return <></>;
  }
}

export default AppShare;
