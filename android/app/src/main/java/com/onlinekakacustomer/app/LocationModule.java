package com.onlinekakacustomer.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.activity.result.IntentSenderRequest;
import androidx.annotation.NonNull;

import com.onlinekakacustomer.BuildConfig;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;

import androidx.core.content.ContextCompat;

import com.onlinekakacustomer.services.FirebaseMessagingService;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.modules.core.PermissionAwareActivity;
import com.facebook.react.modules.core.PermissionListener;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.Task;
import com.onlinekakacustomer.Hyperzod;
import com.onlinekakacustomer.MainApplication;
import com.onlinekakacustomer.helper.PermissionHelper;
import com.onlinekakacustomer.helper.SharedPreferenceUtil;
import com.onlinekakacustomer.services.LocationService;
import com.onlinekakacustomer.services.LocationWorker;

import org.json.JSONObject;

import java.util.Objects;

public class LocationModule extends ReactContextBaseJavaModule {

    private Callback startLocationCallBack;
    private Callback locationEnableCallback;
    private int REQUEST_TURN_DEVICE_LOCATION_ON = 1234;
    boolean isPermissionCheckCompleted = false;

    LocationModule(ReactApplicationContext context) {
        super(context);
        Log.e("Hyperzod", "Module Initialized");

        context.addActivityEventListener(mActivityEventListener);
    }

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            if (requestCode == REQUEST_TURN_DEVICE_LOCATION_ON) {
                switch (resultCode) {
                    case -1:
                        Log.e("Hyperzod", "Location Enabled");
                        // locationEnableCallback.invoke(true);
                        break;
                    default:
                        Log.e("Hyperzod", "Location Not Enabled");

                        // locationEnableCallback.invoke(false);
                        break;
                }
            } else {
                PermissionHelper.getInstance().onActivityResult(activity, requestCode, resultCode, data);
            }
        }
    };

    @NonNull
    @Override
    public String getName() {
        return "LocationModule";
    }

    @ReactMethod
    public void startLocationTracking(Callback callback) {
        MainApplication.sendEvent("Location tracking initiated");
        startLocationCallBack = callback;
        isPermissionCheckCompleted = false;
        PermissionHelper.getInstance().checkPermissions(getCurrentActivity(),
                new PermissionHelper.PermissionCallBack() {
                    @Override
                    public void onPermissionGranted() {
                        startLocationListener();
                        isPermissionCheckCompleted = true;
                    }

                    @Override
                    public void onPermissionDenied() {
                    }
                });
    }

    @ReactMethod
    public void stopLocationTracking(Callback callback) {
        getCurrentActivity().stopService(new Intent(getCurrentActivity(), LocationService.class));
        MainApplication.sendEvent("Location tracking stopped");
        callback.invoke();
    }

    @ReactMethod
    public void startService(ReadableMap driverSettings, ReadableMap driverDetailPayload) {
        Log.e("LocationModule", "driverSettings: " + driverSettings);
        Log.e("LocationModule", "driverDetailPayload: " + driverDetailPayload);

        if (driverDetailPayload.hasKey(SharedPreferenceUtil.TOKEN))
            Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.TOKEN,
                    driverDetailPayload.getString(SharedPreferenceUtil.TOKEN));
        if (driverDetailPayload.hasKey(SharedPreferenceUtil.TEAM_ID))
            Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.TEAM_ID,
                    driverDetailPayload.getInt(SharedPreferenceUtil.TEAM_ID));
        if (driverDetailPayload.hasKey(SharedPreferenceUtil.AGENT_ID))
            Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.AGENT_ID,
                    driverDetailPayload.getString(SharedPreferenceUtil.AGENT_ID));
        Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.ON_DUTY, true);

        if (driverSettings.hasKey(SharedPreferenceUtil.URL))
            Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.URL,
                    driverSettings.getString(SharedPreferenceUtil.URL));
        if (driverSettings.hasKey(SharedPreferenceUtil.LOCATION_INTERVAL))
            Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.LOCATION_INTERVAL,
                    driverSettings.getInt(SharedPreferenceUtil.LOCATION_INTERVAL));
        // Log.e("preferenceTest", "" +
        // Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING,
        // false));

        if (!Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING, false)) {
            // Log.e("preferenceTest", "insideIfCondition->> " +
            // Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING,
            // false));
            MainApplication.sendEvent("Location tracking initiated");
            isPermissionCheckCompleted = false;
            PermissionHelper.getInstance().checkPermissions(getCurrentActivity(),
                    new PermissionHelper.PermissionCallBack() {
                        @Override
                        public void onPermissionGranted() {
                            isPermissionCheckCompleted = true;
                            // Log.e("preferenceTest", "permissionGranted->> " +
                            // Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING,
                            // false));
                            startLocationListener();
                        }

                        @Override
                        public void onPermissionDenied() {
                        }
                    });
        }

    }

    // @ReactMethod
    // public void getAppVersionInfo(Callback callback){
    // String versionName = BuildConfig.VERSION_NAME;
    // int versionCode = BuildConfig.VERSION_CODE;
    // // Invoke the callback with the result
    // WritableMap result = Arguments.createMap();
    // result.putString("versionName", versionName);
    // result.putInt("versionCode", versionCode);
    // callback.invoke(result);
    // }

    @ReactMethod
    public void stopSound() {
        FirebaseMessagingService.stopSound(getCurrentActivity());
    }

    @ReactMethod
    public void stopService(ReadableMap driverSettings, ReadableMap driverDetailPayload) {
        Log.e("LocationModule", "stopService driverSettings: " + driverSettings);
        Log.e("LocationModule", "stopService driverDetailPayload: " + driverDetailPayload);
        if (Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING, false)) {
            Log.e("LocationModule", "Location stopped");
            Hyperzod.getPreferenceUtil().setValue(SharedPreferenceUtil.ON_DUTY, false);
            MainApplication.updateStopLocation();
            getCurrentActivity().stopService(new Intent(getCurrentActivity(), LocationService.class));
            MainApplication.sendEvent("Location tracking stopped");
            LocationWorker.stopLocationWork(getReactApplicationContext());
        }
    }

    @ReactMethod
    public void init() {
        PermissionHelper.getInstance().disableBatteryOptimization(getReactApplicationContext());
    }

    @Override
    public void initialize() {
        super.initialize();
        try {
            if (Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING, false)) {
                startLocationTracking(null);
            }
        } catch (Exception e) {

        }
    }

    @ReactMethod
    public void isLocationTracking(Callback callback) {
        callback.invoke(Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.IS_LOCATION_UPDATING, false));
    }

    private void startLocationListener() {
        Log.e("LocationModule", "Location started");
        MainApplication.sendEvent("Location service method called");
        // PermissionHelper.getInstance().disableBatteryOptimization(getReactApplicationContext());
        Toast.makeText(getCurrentActivity(), "Location tracking started!!!", Toast.LENGTH_SHORT).show();
        ContextCompat.startForegroundService(getCurrentActivity(),
                new Intent(getCurrentActivity(), LocationService.class));
        if (null != startLocationCallBack) {
            startLocationCallBack.invoke(true);
        }
        LocationWorker.startLocationWork(getReactApplicationContext());
    }

    @ReactMethod
    public void enableLocation() {
        // this.locationEnableCallback = callback;
        LocationRequest locationRequest = LocationRequest.create()
                .setInterval(10 * 1000)
                .setFastestInterval(2 * 1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        LocationServices
                .getSettingsClient(getCurrentActivity())
                .checkLocationSettings(builder.build())
                .addOnSuccessListener(getCurrentActivity(), (LocationSettingsResponse response) -> {
                    // callback.invoke(true);
                })
                .addOnFailureListener(getCurrentActivity(), exception -> {
                    if (exception instanceof ResolvableApiException) {
                        try {
                            ((ResolvableApiException) exception).startResolutionForResult(getCurrentActivity(),
                                    REQUEST_TURN_DEVICE_LOCATION_ON);
                        } catch (Exception e) {
                            Log.d("Locatio", "enableLocationSettings: " + exception);
                        }
                    }
                });
    }

    @ReactMethod
    public void checkLocationStatus() {
        MainApplication.sendLocationRequest("locationStatus", checkLocationServices(getCurrentActivity()));
    }

    @ReactMethod
    public void checkLocationPermission(Callback permissionCallback) {
        Log.e("LocationModule", "isPermissionCheckCompleted " + isPermissionCheckCompleted);

        // if (isPermissionCheckCompleted) {
        isPermissionCheckCompleted = false;
        PermissionHelper.getInstance().checkPermissions(getCurrentActivity(),
                new PermissionHelper.PermissionCallBack() {
                    @Override
                    public void onPermissionGranted() {
                        isPermissionCheckCompleted = true;
                        if (permissionCallback != null) {
                            permissionCallback.invoke(true);
                        }
                    }

                    @Override
                    public void onPermissionDenied() {
                        if (permissionCallback != null) {
                            permissionCallback.invoke(false);
                        }
                    }
                });
        // }
    }

    @ReactMethod
    public void showAppLocationPermissionSetting() {
        Log.e("LocationModule", "showAppLocationPermissionSetting ");

        // if (isPermissionCheckCompleted) {
        PermissionHelper.getInstance().loadPermissionPage(getCurrentActivity());
        // }
    }

    @ReactMethod
    public void isBackgroundLocationEnabled(Callback callback) {
        callback.invoke(PermissionHelper.getInstance().checkBackgroundLocationPermissions(getCurrentActivity()));
    }

    @ReactMethod
    public void isBatteryOptimizationEnabled(Callback callback) {
        callback.invoke(PermissionHelper.getInstance().isBatteryOptimizationEnabled(getCurrentActivity()));
    }

    @ReactMethod
    public void isActivityPermissionEnabled(Callback callback) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            callback.invoke(PermissionHelper.getInstance().checkActivityPermission(getCurrentActivity()));
        } else {
            callback.invoke(true);
        }
    }

    private boolean checkLocationServices(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                if (context == null) {
                    return true;
                }
                return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isLocationEnabled();
            } else {
                int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                        Settings.Secure.LOCATION_MODE_OFF);
                return (mode != Settings.Secure.LOCATION_MODE_OFF);
            }
        } catch (Exception e) {
            Log.e("LocationModule", e.getMessage());
        }
        return true;
    }

}
