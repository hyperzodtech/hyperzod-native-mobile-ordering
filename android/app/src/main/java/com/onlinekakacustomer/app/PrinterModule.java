package com.onlinekakacustomer.app;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.dantsu.escposprinter.connection.DeviceConnection;
import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnection;
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections;
import com.dantsu.escposprinter.textparser.PrinterTextParserImg;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.dantsu.escposprinter.*;
import com.izettle.html2bitmap.Html2Bitmap;
import com.izettle.html2bitmap.content.WebViewContent;
import com.onlinekakacustomer.async.AsyncEscPosPrinter;
import com.onlinekakacustomer.helper.BluetoothPermissionHelper;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
// import java.text.SimpleDateFormat;
// import java.util.Date;

public class PrinterModule extends ReactContextBaseJavaModule {

    String printText = "";
    String paperSize = "";
    public static String noPrinterTitle = "No printer title";
    public static String noPrinterMsg = "No printer Desc";
    ProgressDialog pd;
    BluetoothPermissionHelper BluetoothPermissionHelper;
    Executor executor = Executors.newSingleThreadExecutor();

    PrinterModule(ReactApplicationContext context) {
        super(context);
        Log.e("Hyperzod", "Module Initialized");

    }

    @NonNull
    @Override
    public String getName() {
        return "PrinterModule";
    }

    @ReactMethod
    public void printService(ReadableMap json) {
        Log.e("BluetoothHelper", "printService");

        // Check if Bluetooth is enabled
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Log.e("BluetoothHelper", "Bluetooth is not supported on this device");
            // Handle the case where Bluetooth is not supported
            return;
        }

        if (!bluetoothAdapter.isEnabled()) {
            Toast.makeText(getCurrentActivity(), "Bluetooth is not enabled", Toast.LENGTH_SHORT).show();
            return;
        }

        // Bluetooth is enabled, proceed with printing
        BluetoothPermissionHelper = new BluetoothPermissionHelper(getCurrentActivity());
        BluetoothPermissionHelper.checkPermissions(() -> {
            Log.e("PrinterModule", "printerSettings: " + json.getString("html"));
            Log.e("BluetoothHelper", "printService granted");
            this.printText = json.getString("html");
            this.paperSize = json.getString("paperSize");
            // browseBluetoothDevice()
            executor.execute(this::browseBluetoothDevice);
        });
    }

    // @ReactMethod
    // public void enableBluetoothPermission(/* ReadableMap json */) {
    // Log.e("BluetoothHelper", "printService");
    // BluetoothPermissionHelper bluetoothPermissionHelper = new
    // BluetoothPermissionHelper(getCurrentActivity());
    // bluetoothPermissionHelper.checkPermissions(() -> {
    // });
    // }

    public static final int PERMISSION_BLUETOOTH = 1;

    private BluetoothConnection selectedDevice;

    @SuppressLint("MissingPermission")
    public void browseBluetoothDevice() {
        final BluetoothConnection[] bluetoothDevicesList = (new BluetoothPrintersConnections()).getList();

        if (bluetoothDevicesList != null) {

            // Log.e("PrinterModule", "deviceavailable");
            /*
             * final String[] items = new String[bluetoothDevicesList.length + 1];
             * items[0] = "Default printer";
             * int i = 0;
             * for (BluetoothConnection device : bluetoothDevicesList) {
             * items[++i] = device.getDevice().getName();
             * }
             * AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
             * alertDialog.setTitle("Bluetooth printer selection");
             * alertDialog.setItems(items, new DialogInterface.OnClickListener() {
             * 
             * @Override
             * public void onClick(DialogInterface dialogInterface, int i) {
             * int index = i - 1;
             * if(index == -1) {
             * selectedDevice = null;
             * } else {
             * selectedDevice = bluetoothDevicesList[index];
             * printBluetooth();
             * }
             * }
             * });
             */

            final List<String> items = new ArrayList<>();
            int i = 0;
            for (BluetoothConnection device : bluetoothDevicesList) {
                // if (ActivityCompat.checkSelfPermission(getCurrentActivity(),
                // Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED)
                // {
                // // TODO: Consider calling
                // // ActivityCompat#requestPermissions
                // // here to request the missing permissions, and then overriding
                // // public void onRequestPermissionsResult(int requestCode, String[]
                // permissions,
                // // int[] grantResults)
                // // to handle the case where the user grants the permission. See the
                // documentation
                // // for ActivityCompat#requestPermissions for more details.
                // return;
                // }
                if (null != device.getDevice().getName() && items.size() == 0) {
                    items.add(device.getDevice().getName());
                }
            }
            if (items.size() == 0) {
                // No devices
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getCurrentActivity());
                alertDialog.setTitle("No Printer");// noPrinterMsg
                AlertDialog alert = alertDialog.create();
                alert.show();
            } else if (items.size() == 1) {
                selectedDevice = bluetoothDevicesList[0];
                printBluetooth();
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getCurrentActivity());
                alertDialog.setTitle("Bluetooth printer selection");
                String[] chars = items.toArray(new String[0]);
                alertDialog.setItems(chars, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int index = i;
                        if (index == -1) {
                            selectedDevice = null;
                        } else {
                            selectedDevice = bluetoothDevicesList[index];
                            printBluetooth();
                        }
                    }
                });

                AlertDialog alert = alertDialog.create();
                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getCurrentActivity());
            alertDialog.setTitle("No Printer");
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    }

    // 7052
    public void printBluetooth() {
        Log.e("PrinterModule", "printBluetooth");
        // if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) !=
        // PackageManager.PERMISSION_GRANTED) {
        // ActivityCompat.requestPermissions(this, new
        // String[]{Manifest.permission.BLUETOOTH}, MainActivity.PERMISSION_BLUETOOTH);
        // } else {
        // new
        // AsyncBluetoothEscPosPrint(this).execute(this.getAsyncEscPosPrinter(selectedDevice));
        // }

        // this.pd = new ProgressDialog(getCurrentActivity());
        // this.pd.setMessage("Printing..");
        // this.pd.setCancelable(false);
        // this.pd.setIndeterminate(false);
        // this.pd.show();

        final Context context = getCurrentActivity();
        // this.pd = new ProgressDialog(getCurrentActivity());

        // new Thread(new Runnable() {
        // public void run() {
        // pd.setMessage("Printing..");
        // pd.setCancelable(false);
        // pd.setIndeterminate(false);
        // pd.show();
        String html = "<html><body>" + printText + "</body><html>";
        // long currentTimestamp = System.currentTimeMillis();
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // Date date = new Date(currentTimestamp);
        // String formattedDateTime = sdf.format(date);
        // String html = "<html><body>" + "<h1>" + formattedDateTime + "</h1>"
        // + printText + "</body><html>";

        // String logTest = "printTestTest";
        // Log.d(logTest, printText);
        try {
            // Log.e("PrinterModule", "selectedDevice--->" +
            // selectedDevice.getDevice().getName());
            // Log.e("PrinterModule", "isPaperSize80mm " + paperSize.equals("80mm"));
            
            // Set parameters based on the paper size
            float paperWidthMm = paperSize.equals("80mm") ? 80f : 48f;
            int charsPerLine = paperSize.equals("80mm") ? 48 : 32;
            // int bitmapWidthPx = paperSize.equals("80mm") ? 576 : 384; // 1mm = ~2.4px at 203 DPI
            List<Bitmap> segments = new ArrayList<Bitmap>();
            // EscPosPrinter printer = new EscPosPrinter(selectedDevice, 203, 48f, 32);
            EscPosPrinter printer = new EscPosPrinter(selectedDevice, 203, paperWidthMm, charsPerLine);
            printer.useEscAsteriskCommand(false);
            final Bitmap decodedByte = new Html2Bitmap.Builder().setContext(context)
                    .setBitmapWidth(paperSize.equals("80mm") ? 576 : printer.getPrinterWidthPx()).setContent(WebViewContent.html(html)).build()
                    .getBitmap();

            int width = decodedByte.getWidth();
            int height = decodedByte.getHeight();

            StringBuilder textToPrint = new StringBuilder();
            for (int y = 0; y < height; y += 256) {
                Bitmap bitmap = Bitmap.createBitmap(decodedByte, 0, y, width,
                        (y + 256 >= height) ? height - y : 256);
                textToPrint.append("[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, bitmap)
                        + "</img>\n");
            }
            // textToPrint.append("[C]Printed!!!\n");
            printer.printFormattedTextAndCut(textToPrint.toString());
            printer.disconnectPrinter();

            // new Handler(Looper.getMainLooper(),new Runnable(){
            // @Override
            // public void run() {
            // pd.dismiss();
            // pd = null;
            // }
            // });

            // new Handler(Looper.getMainLooper()).post(() -> {
            // pd.dismiss();
            // pd = null;
            // });

        } catch (Exception e) {
            e.printStackTrace();
            // new Handler(Looper.getMainLooper()).post(() -> {
            // pd.dismiss();
            // pd = null;
            // });
        }
        // }
        // }).start();

    }

    /**
     * Asynchronous printing
     */
    @SuppressLint("SimpleDateFormat")
    public AsyncEscPosPrinter getAsyncEscPosPrinter(DeviceConnection printerConnection) {
        // SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at'
        // HH:mm:ss");
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 48f, 32);
        return printer.setTextToPrint(printText);
        // return printer.setTextToPrint(
        // "[L]\n" +
        // "[C]<u><font size='big'>ORDER NÂ°045</font></u>\n" +
        // "[L]\n" +
        // "[C]<u type='double'>" + format.format(new Date()) + "</u>\n" +
        // "[C]\n" +
        // "[C]================================\n" +
        // "[L]\n" +
        // "[L]<b>BEAUTIFUL SHIRT</b>[R]9.99â‚¬\n" +
        // "[L] + Size : S\n" +
        // "[L]\n" +
        // "[L]<b>AWESOME HAT</b>[R]24.99â‚¬\n" +
        // "[L] + Size : 57/58\n" +
        // "[L]\n" +
        // "[C]--------------------------------\n" +
        // "[R]TOTAL PRICE :[R]34.98â‚¬\n" +
        // "[R]TAX :[R]4.23â‚¬\n" +
        // "[L]\n" +
        // "[C]================================\n" +
        // "[L]\n" +
        // "[L]<u><font color='bg-black' size='tall'>Customer :</font></u>\n" +
        // "[L]Raymond DUPONT\n" +
        // "[L]5 rue des girafes\n" +
        // "[L]31547 PERPETES\n" +
        // "[L]Tel : +33801201456\n" +
        // "\n" +
        // "[C]<barcode type='ean13' height='10'>831254784551</barcode>\n" +
        // "[L]\n" +
        // "[C]<qrcode size='20'>http://www.developpeur-web.dantsu.com/</qrcode>\n"
        // );
    }

}