package com.onlinekakacustomer;

import android.app.Application;
import android.content.Context;

import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.onlinekakacustomer.app.LocationPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import com.onlinekakacustomer.app.PrinterPackage;
import com.onlinekakacustomer.repository.LocationRepository;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import android.location.Location;
import android.webkit.WebView;

public class MainApplication extends Application implements ReactApplication {
    public static final String keyHeartBeat = "HeartBeat";
    public static final String keyServiceCreated = "ServiceCreated";
    public static final String keyServiceDestroyed = "ServiceDestroyed";
    public static final String keyServiceRestarted = "ServiceRestarted";
    public static final String keyBootCompleted = "BootCompleted";
    public static final String keyServiceRestartedWorkManager = "ServiceRestartedWorkManager";
    public static final String workManagerUpdated = "WorkManagerUpdated";
    private static LocationRepository locationRepository;
    private final ReactNativeHost mReactNativeHost =
            new ReactNativeHost(this) {
                @Override
                public boolean getUseDeveloperSupport() {
                    return BuildConfig.DEBUG;
                }

                @Override
                protected List<ReactPackage> getPackages() {
                    @SuppressWarnings("UnnecessaryLocalVariable")
                    List<ReactPackage> packages = new PackageList(this).getPackages();
                    // Packages that cannot be autolinked yet can be added manually here, for example:
                    // packages.add(new MyReactNativePackage());
                    packages.add(new LocationPackage());
                    packages.add(new PrinterPackage());
                    return packages;
                }

                @Override
                protected String getJSMainModuleName() {
                    return "index";
                }
            };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Hyperzod.init(this);
        locationRepository = LocationRepository.getInstance(this);
        SoLoader.init(this, /* native exopackage */ false);
        WebView.setWebContentsDebuggingEnabled(true);
        initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
    }

    /**
     * Loads Flipper in React Native templates. Call this in the onCreate method with something like
     * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
     *
     * @param context
     * @param reactInstanceManager
     */
    private static void initializeFlipper(
            Context context, ReactInstanceManager reactInstanceManager) {
        if (BuildConfig.DEBUG) {
            try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
                Class<?> aClass = Class.forName("com.onlinekakacustomer.ReactNativeFlipper");
                aClass
                        .getMethod("initializeFlipper", Context.class, ReactInstanceManager.class)
                        .invoke(null, context, reactInstanceManager);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }


    public static void sendEvent(String message) {
        locationRepository.sendEvent(message);
    }

    public static void sendLocationRequest(String event, boolean isEnabled) {
        locationRepository.sendLocationRequest(event, isEnabled);
    }

    public static void updateLocation(Location location) {
        locationRepository.updateLocation(location);
    }

    public static void updateStopLocation() {
        locationRepository.updateStopLocation();
    }

    public static void logTime(String key) {
        locationRepository.logTime(key);
    }

    public static void updateDeviceInfo() {
        locationRepository.updateDeviceInfo();
    }

}
