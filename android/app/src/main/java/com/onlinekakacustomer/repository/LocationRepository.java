package com.onlinekakacustomer.repository;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onlinekakacustomer.Hyperzod;
import com.onlinekakacustomer.MainApplication;
import com.onlinekakacustomer.helper.SharedPreferenceUtil;
import com.onlinekakacustomer.helper.Utils;
import com.onlinekakacustomer.network.request.LocationUpdateRequest;
import com.onlinekakacustomer.network.response.LocationUpdateResponse;
import com.onlinekakacustomer.network.retrofit.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationRepository {

    public static Calendar lastUpdatedTime;
    public static LocationRepository instance;
    private JSONArray locArray = new JSONArray();
    private Context context;
    private Location lastUpdatedLocation;

    public static LocationRepository getInstance(Context context) {
        if (null == instance)
            instance = new LocationRepository(context.getApplicationContext());
        return instance;
    }

    public LocationRepository(Context context) {
        this.context = context;
    }

    private void invokeSendEvent(ReactContext reactContext, String eventName, Object data) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, data);
    }

    public void sendEvent(final String eventName, final Object data) {
        ReactApplication reactApplication = (ReactApplication) context.getApplicationContext();
        ReactNativeHost mReactNativeHost = reactApplication.getReactNativeHost();
        final ReactInstanceManager reactInstanceManager = mReactNativeHost.getReactInstanceManager();
        ReactContext reactContext = reactInstanceManager.getCurrentReactContext();
        if (reactContext == null) {
            reactInstanceManager.addReactInstanceEventListener(new ReactInstanceManager.ReactInstanceEventListener() {
                @Override
                public void onReactContextInitialized(ReactContext reactContext) {
                    invokeSendEvent(reactContext, eventName, data);
                    reactInstanceManager.removeReactInstanceEventListener(this);
                }
            });
            if (!reactInstanceManager.hasStartedCreatingInitialContext()) {
                reactInstanceManager.createReactContextInBackground();
            }
        } else {
            invokeSendEvent(reactContext, eventName, data);
        }
    }


    public void updateLocation(Location location) {
        lastUpdatedLocation = location;
        try {
            WritableMap array = Utils.mapForLocation(location);
            sendEvent("location", array);
            updateLocationFirebase(location);
            updateLocationApi(location);
        } catch (Exception e) {
            Log.e("Error", "e: " + e.getMessage());
            sendEvent("error", e.getMessage());
            MainApplication.sendEvent(e.getMessage());
        }
    }

    public void updateStopLocation() {
        if (null == lastUpdatedLocation)
            return;
        try {
            WritableMap array = Utils.mapForLocation(lastUpdatedLocation);
            sendEvent("location", array);
            updateLocationFirebase(lastUpdatedLocation);
            updateLocationApi(lastUpdatedLocation);
        } catch (Exception e) {
            Log.e("Error", "e: " + e.getMessage());
            sendEvent("error", e.getMessage());
            MainApplication.sendEvent(e.getMessage());
        }
    }

    public void sendEvent(String event) {
        locArray.put(event);
        updateEvent();
        sendEvent("events", event);
    }

    public void sendLocationRequest(String event, boolean isEnabled) {
        sendEvent(event, isEnabled);
    }

    private void updateLocationFirebase(Location location) {
        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(getDeviceId() + "");
            Map<String, String> mapLocation = new HashMap<>();
            if (null != location) {
                lastUpdatedTime = Calendar.getInstance();
                String time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(lastUpdatedTime.getTime());
                mapLocation.put("lat", location.getLatitude() + "");
                mapLocation.put("lng", location.getLongitude() + "");
                mapLocation.put("time", time);
            }
            myRef.child("location").setValue(mapLocation);
        } catch (Exception e) {
            e.printStackTrace();
            sendEvent("error", e.getMessage());
            MainApplication.sendEvent(e.getMessage());
        }
    }

    public void updateDeviceInfo() {
        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(getDeviceId() + "");
            Map<String, String> mapDeviceInfo = new HashMap<>();
            mapDeviceInfo.put("Device", Build.DEVICE + "");
            mapDeviceInfo.put("Brand", Build.BRAND);
            mapDeviceInfo.put("Model", Build.MODEL);
            mapDeviceInfo.put("SDK", Build.VERSION.SDK_INT + "");
            mapDeviceInfo.put("Manufacture", Build.MANUFACTURER);
            mapDeviceInfo.put("Base", Build.VERSION_CODES.BASE + "");
            mapDeviceInfo.put("Board", Build.BOARD + "");
            mapDeviceInfo.put("VersionCode", Build.VERSION.RELEASE + "");
            myRef.child("DeviceInfo").setValue(mapDeviceInfo);
        } catch (Exception e) {
            e.printStackTrace();
            sendEvent("error", e.getMessage());
            MainApplication.sendEvent(e.getMessage());
        }
    }

    private void updateEvent() {
        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(getDeviceId() + "");
            myRef.child("Events").setValue(locArray.toString());
        } catch (Exception e) {
            e.printStackTrace();
            sendEvent("error", e.getMessage());
        }
    }

    public void logTime(String key) {
        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(getDeviceId() + "");
            String time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
            myRef.child(key).setValue(time);
        } catch (Exception e) {
            e.printStackTrace();
            sendEvent("error", e.getMessage());
            MainApplication.sendEvent(e.getMessage());
        }
    }

    private String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    private void updateLocationApi(Location location) {

        LocationUpdateRequest locationUpdateRequest = new LocationUpdateRequest();
        List<Double> locationList = new ArrayList<>();
        locationList.add(location.getLongitude());
        locationList.add(location.getLatitude());
        locationUpdateRequest.setLocation(locationList);
        locationUpdateRequest.setAgentId(Hyperzod.getPreferenceUtil().getStringValue(SharedPreferenceUtil.AGENT_ID, ""));
        locationUpdateRequest.setTeamId(Hyperzod.getPreferenceUtil().getIntValue(SharedPreferenceUtil.TEAM_ID, 0));
        locationUpdateRequest.setOnDuty(Hyperzod.getPreferenceUtil().getBoolanValue(SharedPreferenceUtil.ON_DUTY, false));

        Hyperzod.getRetrofitClient()
                .updateLocation(Hyperzod.getPreferenceUtil().getStringValue(SharedPreferenceUtil.URL, ""), locationUpdateRequest)
                .enqueue(new Callback<LocationUpdateResponse>() {
                    @Override
                    public void onResponse(Call<LocationUpdateResponse> call, Response<LocationUpdateResponse> response) {
                        Log.e("LocationApi", "Response >>>> " + response.message());
                    }

                    @Override
                    public void onFailure(Call<LocationUpdateResponse> call, Throwable t) {
                        Log.e("LocationApi", "Error >>>> " + t.getMessage());
                    }
                });
    }

}

