package com.onlinekakacustomer.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.onlinekakacustomer.MainActivity;
import com.onlinekakacustomer.R;
import com.onlinekakacustomer.services.LocationUpdatesBroadcastReceiver;

public class NotificationUtils {

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static Notification notification(Context context) {
        createNotificationChannel(context);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ok.location");
        try {
            builder
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle("Location Service")
                    .setContentText("Location service is running")
                    .setContentIntent(getPendingIntent(context));
            builder.setAutoCancel(true);
        } catch (Exception unused2) {
        }
        return builder.build();
    }

    public static void showNormalNotification(Context context) {
        try {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ok.location");
            builder
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle("Location Service")
                    .setContentText("Location service is running")
                    .setContentIntent(getPendingIntent(context));
            builder.setAutoCancel(true);
            Notification notification = builder.build();
            notification.flags = Notification.FLAG_ONGOING_EVENT;
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, notification);
        } catch (Exception unused2) {
        }
    }

    public static void showNormalNotification(Context context, String title, String message, boolean cancelSound, Uri rawPathUri) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel(context, cancelSound, rawPathUri);
            }
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ok.custom_sound");
            builder
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(getPendingIntent(context));
            builder.setAutoCancel(true);
            builder.setVibrate(new long[]{0L});
            Notification notification = builder.build();
//            if (null != rawPathUri) {
//                builder.setSound(rawPathUri);
//                notification.flags = Notification.FLAG_INSISTENT;
//            }
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        } catch (Exception unused2) {
        }
    }

    private static PendingIntent getPendingIntent(Context context) {
        TaskStackBuilder create = TaskStackBuilder.create(context);
        Intent intent = new Intent(context, MainActivity.class);
        create.addNextIntent(intent);
        return create.getPendingIntent(1, PendingIntent.FLAG_MUTABLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void createNotificationChannel(Context context) {
        NotificationChannel notificationChannel = new NotificationChannel("ok.location", "ok", NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(false);
        notificationChannel.enableVibration(false);
        notificationChannel.setSound(null, null);
        notificationChannel.setLockscreenVisibility(0);
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(notificationChannel);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void createNotificationChannel(Context context, boolean cancelSound, Uri rawPathUri) {
        NotificationChannel notificationChannel = new NotificationChannel("ok.custom_sound", "ok.custom_sound_channel", NotificationManager.IMPORTANCE_DEFAULT);
//        if (rawPathUri != null) {
//            notificationChannel.setSound(rawPathUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
//        }

//        if (cancelSound) {
            notificationChannel.setSound(null, null);
            notificationChannel.setVibrationPattern(new long[]{0L});
//        }
//        else {
//            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            notificationChannel.setSound(soundUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
//        }
        notificationChannel.setLockscreenVisibility(0);
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(notificationChannel);
    }

    public static void updateNotification(Context context, String content) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ok.location");
        builder
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("Location Service")
                .setContentText(content)
                .setContentIntent(getPendingIntent(context));
        Notification notification = builder.build();
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

    public static PendingIntent getPendingIntentBroadcast(Context context) {
        Intent intent = new Intent(context, LocationUpdatesBroadcastReceiver.class);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_MUTABLE);
    }

}
