package com.onlinekakacustomer.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.util.Arrays;
import androidx.core.content.ContextCompat;

import com.intentfilter.androidpermissions.PermissionManager;
import com.intentfilter.androidpermissions.models.DeniedPermission;
import com.intentfilter.androidpermissions.models.DeniedPermissions;
import java.util.Collections;

public class BluetoothPermissionHelper {
    private Activity context;
    private static final String tag = "BluetoothHelper";

    public interface PermissionCallBack {
        void onPermissionGranted();
    }

    private PermissionCallBack permissionCallBack;

    public BluetoothPermissionHelper(Activity context) {
        this.context = context;
    }

    public void checkPermissions(PermissionCallBack permissionCallBack) {
        this.permissionCallBack = permissionCallBack;
        managePermission();
    }

    private void managePermission() {
        if (null != context) {
            Log.e(tag, "Bluetooth permission requested");
            if (!checkBluetoothPermissions(context)) {
                requestBluetoothPermission();
            } else {
                if (null != permissionCallBack) {
                    Log.e(tag, "Bluetooth permission granted");
                    permissionCallBack.onPermissionGranted();
                }
            }
        } else {
            Log.e(tag, "Context is not available");
        }
    }

    private void requestBluetoothPermission() {
        PermissionManager permissionManager = PermissionManager.getInstance(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            // permissionManager.checkPermissions(Collections.singletonList(Manifest.permission.BLUETOOTH_CONNECT),
            // new PermissionManager.PermissionRequestListener() {
            permissionManager.checkPermissions(
                    Arrays.asList(Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN),
                    new PermissionManager.PermissionRequestListener() {
                        @Override
                        public void onPermissionGranted() {
                            managePermission();
                            Log.e(tag, "Permission granted");
                        }

                        @Override
                        public void onPermissionDenied(DeniedPermissions deniedPermissions) {
                            Log.e(tag, "Bluetooth permission rationale");
                            for (DeniedPermission deniedPermission : deniedPermissions) {
                                Log.e(tag, "deniedPermission: " + deniedPermission.toString());
                                if (deniedPermission.shouldShowRationale()) {

                                }
                            }
                        }
                    });
        } else {
            managePermission();
        }
    }

    // private boolean checkBluetoothPermissions(Context context) {
    // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
    // return ContextCompat.checkSelfPermission(context,
    // Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED;
    // } else {
    // return true;
    // }
    // }
    private boolean checkBluetoothPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            return ContextCompat.checkSelfPermission(context,
                    Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context,
                            Manifest.permission.BLUETOOTH_SCAN) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

}
