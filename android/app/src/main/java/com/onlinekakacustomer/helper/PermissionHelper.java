package com.onlinekakacustomer.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.modules.core.PermissionAwareActivity;
import com.facebook.react.modules.core.PermissionListener;
import com.onlinekakacustomer.MainApplication;

public class PermissionHelper implements PermissionListener {
    private Activity context;
    public static int REQUEST_CODE_LOCATION_SOURCE_SETTINGS = 1234;
    public static final int REQUEST_CODE_LOCATION_PERMISSION = 1122;
    public static final int REQUEST_CODE_BACKGROUND_LOCATION_PERMISSION = 1133;
    public static final int REQUEST_CODE_ACTIVITY_PERMISSION = 1144;
    private static PermissionHelper permissionHelper;

    public static PermissionHelper getInstance() {
        if (permissionHelper == null) {
            permissionHelper = new PermissionHelper();
        }
        return permissionHelper;
    }

    public interface PermissionCallBack {
        void onPermissionGranted();

        void onPermissionDenied();
    }

    private PermissionCallBack permissionCallBack;

    public PermissionHelper() {
    }

    public void checkPermissions(Activity activity, PermissionCallBack permissionCallBack) {
        context = activity;
        this.permissionCallBack = permissionCallBack;
        managePermission();
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_LOCATION_SOURCE_SETTINGS) {
            MainApplication.sendEvent("Location service granted");

        }
    }

    private void managePermission() {
        Log.e("permissionHelper", "managePermission " + context);
        if (null != context) {
            Log.e("permissionHelper", "inside condition " + (checkLocationPermissions(context)));
            // if (!checkLocationServices(context)) {
            // MainApplication.sendEvent("Location service requested");
            // requestLocationServices();
            // } else
            if (!checkLocationPermissions(context)) {
                requestLocationPermission();
            }
            // else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && //
            // !checkBackgroundLocationPermissions()) {
            // requestBackgroundLocationPermission();
            // }
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !checkActivityPermission()) {
                requestActivityPermission();
            } else {
                // Log.e("permissionHelper", "inside else condition ");
                if (null != permissionCallBack) {
                    permissionCallBack.onPermissionGranted();
                }
            }
        } else {
            MainApplication.sendEvent("Context null");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void requestActivityPermission() {

        MainApplication.sendEvent("Location permission ACTIVITY_RECOGNITION requested");

        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                Manifest.permission.ACTIVITY_RECOGNITION)) {
            if (null != permissionCallBack) {
                permissionCallBack.onPermissionGranted();
            }
        } else {
            PermissionAwareActivity activity = (PermissionAwareActivity) context;
            activity.requestPermissions(new String[] { Manifest.permission.ACTIVITY_RECOGNITION },
                    REQUEST_CODE_ACTIVITY_PERMISSION, this);
        }
    }

    private void requestLocationPermission() {
        MainApplication.sendEvent("Location permission ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION requested");
        Log.e("permissionHelper", "requestLocationPermission");
        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(context,
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {
            MainApplication.sendLocationRequest("PermissionSettings", true);
            Log.e("permissionHelper", "requestLocationPermission rationale");
            if (null != permissionCallBack) {
                permissionCallBack.onPermissionDenied();
            }
        } else {
            PermissionAwareActivity activity = (PermissionAwareActivity) context;
            activity.requestPermissions(
                    new String[] { Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    REQUEST_CODE_LOCATION_PERMISSION, this);
        }
    }

    public void loadPermissionPage(Activity context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivityForResult(intent, REQUEST_CODE_LOCATION_SOURCE_SETTINGS);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void requestBackgroundLocationPermission() {
        MainApplication.sendEvent("Location permission ACCESS_BACKGROUND_LOCATION requested");
        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
            Log.e("permissionHelper", "Background rationale ");
            PermissionAwareActivity activity = (PermissionAwareActivity) context;
            activity.requestPermissions(new String[] { Manifest.permission.ACCESS_BACKGROUND_LOCATION },
                    REQUEST_CODE_BACKGROUND_LOCATION_PERMISSION, this);
            if (null != permissionCallBack) {
                permissionCallBack.onPermissionDenied();
            }
        } else {
            PermissionAwareActivity activity = (PermissionAwareActivity) context;
            activity.requestPermissions(new String[] { Manifest.permission.ACCESS_BACKGROUND_LOCATION },
                    REQUEST_CODE_BACKGROUND_LOCATION_PERMISSION, this);
        }
    }

    private boolean checkLocationServices(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (context == null) {
                return true;
            }
            return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isLocationEnabled();
        } else {
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return (mode != Settings.Secure.LOCATION_MODE_OFF);
        }
    }

    private boolean checkLocationPermissions(Context context) {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private boolean checkActivityPermission() {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public boolean checkActivityPermission(Context context) {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkBackgroundLocationPermissions() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkBackgroundLocationPermissions(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isBatteryOptimizationEnabled(Context context) {
        if(context == null){
            return true;
        }
        String packageName = context.getPackageName();
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return powerManager == null || powerManager.isIgnoringBatteryOptimizations(packageName);
    }

    public void disableBatteryOptimization(Context context) {
        Intent intent = new Intent();
        String packageName = context.getPackageName();
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (powerManager == null || powerManager.isIgnoringBatteryOptimizations(packageName)) {
            return;
        }
        intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
        intent.setData(Uri.parse("package:" + packageName));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    @Override
    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOCATION_PERMISSION:
                if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    managePermission();
                } else {
                    MainApplication.sendLocationRequest("PermissionSettings", checkLocationServices(context));
                    if (permissionCallBack != null) {
                        permissionCallBack.onPermissionDenied();
                    }
                }
                break;
            case REQUEST_CODE_BACKGROUND_LOCATION_PERMISSION:
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    managePermission();
                } else {
                    MainApplication.sendLocationRequest("PermissionSettings", checkLocationServices(context));
                    if (permissionCallBack != null) {
                        permissionCallBack.onPermissionDenied();
                    }
                }
            case REQUEST_CODE_ACTIVITY_PERMISSION:
                managePermission();
        }

        return true;
    }

}
