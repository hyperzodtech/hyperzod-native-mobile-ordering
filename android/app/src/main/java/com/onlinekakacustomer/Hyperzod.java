package com.onlinekakacustomer;

import android.content.Context;
import android.util.Log;

import com.onlinekakacustomer.helper.SharedPreferenceUtil;
import com.onlinekakacustomer.network.retrofit.ApiInterface;
import com.onlinekakacustomer.network.retrofit.RetrofitClient;

public class Hyperzod {

    private static Hyperzod instance;
    private SharedPreferenceUtil preferenceUtil;

    public static Hyperzod init(Context context) {
        if (null == instance) {
            instance = new Hyperzod(context.getApplicationContext());
        }
        return instance;
    }

    public static Hyperzod getInstance() {
        return instance;
    }

    private Hyperzod(Context context) {
        Log.e("Hyperzod", "Initialized");
        preferenceUtil = SharedPreferenceUtil.getInstance(context);
    }

    public static SharedPreferenceUtil getPreferenceUtil() {
        return Hyperzod.getInstance().preferenceUtil;
    }

    public static ApiInterface getRetrofitClient(){
        return RetrofitClient.getInstance().provideApiInterface();
    }
}
