
package com.onlinekakacustomer.network.request;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class LocationUpdateRequest {

    @SerializedName("team_id")
    private int teamId;
    @SerializedName("agent_id")
    private String agentId;
    @SerializedName("on_duty")
    private boolean onDuty;
    @SerializedName("location")
    private List<Double> location = null;

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public boolean isOnDuty() {
        return onDuty;
    }

    public void setOnDuty(boolean onDuty) {
        this.onDuty = onDuty;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

}
