package com.onlinekakacustomer.network.retrofit;

import com.onlinekakacustomer.network.request.LocationUpdateRequest;
import com.onlinekakacustomer.network.response.LocationUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @POST("{fullUrl}")
    Call<LocationUpdateResponse> updateLocation(@Path(value = "fullUrl", encoded = true) String fullUrl, @Body LocationUpdateRequest locationUpdateRequest);
}
