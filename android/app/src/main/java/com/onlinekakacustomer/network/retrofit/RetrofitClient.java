package com.onlinekakacustomer.network.retrofit;

import com.onlinekakacustomer.Hyperzod;
import com.onlinekakacustomer.helper.SharedPreferenceUtil;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static RetrofitClient instance;

    private RetrofitClient() {
    }

    public static RetrofitClient getInstance() {
        if (null == instance)
            instance = new RetrofitClient();
        return instance;
    }

    private OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addNetworkInterceptor(interceptor).addInterceptor(chain -> {
            Request request = chain
                    .request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer "+Hyperzod.getPreferenceUtil().getStringValue(SharedPreferenceUtil.TOKEN, ""))
                    .build();
            return chain.proceed(request);
        }).build();
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://www.autozod.dev/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    public ApiInterface provideApiInterface() {
        return getRetrofit().create(ApiInterface.class);
    }

}
