package com.onlinekakacustomer;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.GnssStatus;
import android.location.GpsStatus;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.onlinekakacustomer.helper.PermissionHelper;
import com.onlinekakacustomer.services.FirebaseMessagingService;
import com.onlinekakacustomer.services.LocationProviderChangedReceiver;

public class MainActivity extends ReactActivity {
    private BroadcastReceiver receiver;

    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "OnlinekakaCustomer";
    }


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("LocationStatus", "On resume called");
        LocationProviderChangedReceiver.prevLocationStatus = checkLocationServices(this);
        MainApplication.sendLocationRequest("locationStatus", checkLocationServices(this));
        FirebaseMessagingService.stopSound(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.location.PROVIDERS_CHANGED");
        receiver = new LocationProviderChangedReceiver();
        this.getApplicationContext().registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        this.getApplicationContext().unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseMessagingService.stopSound(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
         Log.e("permissionHelper", "onStart called");
        // PermissionHelper.getInstance().checkPermissions(this, () -> {});
    }

    private boolean checkLocationServices(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isLocationEnabled();
        } else {
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return (mode != Settings.Secure.LOCATION_MODE_OFF);
        }
    }
}
