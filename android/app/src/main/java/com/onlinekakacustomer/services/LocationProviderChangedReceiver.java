package com.onlinekakacustomer.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.text.TextUtils;
import android.util.Log;

import com.onlinekakacustomer.MainApplication;

public class LocationProviderChangedReceiver extends BroadcastReceiver {
    public static boolean prevLocationStatus = true;


    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action) && action.equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {

                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean anyLocationProvCurr = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                Log.i("LocationStatus", "Location service status" + anyLocationProvCurr);
                if (prevLocationStatus != anyLocationProvCurr) {
                    MainApplication.sendLocationRequest("locationStatus", anyLocationProvCurr);
                }
                prevLocationStatus = anyLocationProvCurr;
            }
        }

    }

}