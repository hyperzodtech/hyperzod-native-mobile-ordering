package com.onlinekakacustomer.services;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.work.BackoffPolicy;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.onlinekakacustomer.MainApplication;
import com.onlinekakacustomer.helper.SharedPreferenceUtil;
import com.onlinekakacustomer.helper.Utils;

import java.util.concurrent.TimeUnit;

public class LocationWorker extends Worker {
    public static final String LOCATION_UPDATE = "LOCATION_UPDATE";
    public static final String LOCATION_UPDATE_WORK = "LOCATION_UPDATE_WORK";

    public LocationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        MainApplication.logTime(MainApplication.workManagerUpdated);
        if (SharedPreferenceUtil.getInstance(getApplicationContext()).getBoolanValue("RequestingLocationUpdates", false)) {
            if (!Utils.isLocationUpdating()) {
                ContextCompat.startForegroundService(getApplicationContext(), new Intent(getApplicationContext(), LocationService.class));
                MainApplication.logTime(MainApplication.keyServiceRestartedWorkManager);
            }
        }
        return Result.success();
    }

    public static void startLocationWork(Context context) {
        PeriodicWorkRequest periodicSyncDataWork =
                new PeriodicWorkRequest.Builder(LocationWorker.class, 15, TimeUnit.MINUTES)
                        .addTag(LOCATION_UPDATE)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, PeriodicWorkRequest.MIN_BACKOFF_MILLIS, TimeUnit.MILLISECONDS)
                        .build();
        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
                LOCATION_UPDATE_WORK,
                ExistingPeriodicWorkPolicy.KEEP, //Existing Periodic Work policy
                periodicSyncDataWork);
    }

    public static void stopLocationWork(Context context) {
        WorkManager.getInstance(context).cancelAllWorkByTag(LOCATION_UPDATE);
    }

}
