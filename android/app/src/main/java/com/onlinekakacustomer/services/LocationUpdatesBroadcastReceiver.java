package com.onlinekakacustomer.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationResult;
import com.onlinekakacustomer.MainApplication;
import com.onlinekakacustomer.helper.NotificationUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "LUBroadcastReceiver";
    public static final String ACTION_PROCESS_UPDATES = "com.locationrnd" + ".PROCESS_UPDATES";
    static boolean isLocationUpdated = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {

            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {
                    List<Location> locations = result.getLocations();
                    Location firstLocation = locations.get(0);
                    Log.e("LocationModule", "Location Received: "+this.hashCode());
                    MainApplication.updateLocation(firstLocation);
                    String time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                    NotificationUtils.updateNotification(context, "(" + firstLocation.getLatitude() + ", " + firstLocation.getLongitude() + ") " + time);
                }
                if (!isLocationUpdated) {
                    MainApplication.sendEvent("Location update received");
                }
                isLocationUpdated = true;
            }
        }
    }
}
