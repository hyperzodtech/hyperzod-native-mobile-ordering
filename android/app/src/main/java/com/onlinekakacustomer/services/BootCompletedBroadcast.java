package com.onlinekakacustomer.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.onlinekakacustomer.MainApplication;
import com.onlinekakacustomer.helper.SharedPreferenceUtil;

public class BootCompletedBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Toast.makeText(context, "Boot completed", Toast.LENGTH_SHORT).show();
        if (SharedPreferenceUtil.getInstance(context).getBoolanValue("RequestingLocationUpdates", false)) {
            MainApplication.logTime(MainApplication.keyBootCompleted);
            ContextCompat.startForegroundService(context, new Intent(context, LocationService.class));
        }
    }
}
