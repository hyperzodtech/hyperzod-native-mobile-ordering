package com.onlinekakacustomer.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.Priority;
import com.google.android.gms.tasks.Task;
import com.onlinekakacustomer.Hyperzod;
import com.onlinekakacustomer.MainApplication;
import com.onlinekakacustomer.helper.NotificationUtils;
import com.onlinekakacustomer.helper.SharedPreferenceUtil;
import com.onlinekakacustomer.helper.Utils;
import com.onlinekakacustomer.repository.LocationRepository;

import java.util.Calendar;

import javax.inject.Inject;

public class LocationService extends Service {
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private ActivityRecognitionClient mActivityRecognitionClient;
    private Handler locationHandler;
//    private static final int UPDATE_INTERVAL_DEFAULT = 10 * 1000;
//    private int UPDATE_INTERVAL = 10 * 1000;
//    private static final float SMALLEST_DISPLACEMENT = 1.0F;
//    private static final long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL_DEFAULT / 2;
//    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL_DEFAULT * 2;

    private int UPDATE_INTERVAL = 10 * 1000;
    private long FASTEST_UPDATE_INTERVAL = 10 * 1000;
    private long MAX_WAIT_TIME = 10 * 1000 * 2;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("LocationModule", "Service created");
        UPDATE_INTERVAL = Hyperzod.getPreferenceUtil().getIntValue(SharedPreferenceUtil.LOCATION_INTERVAL, 10) * 1000;
        FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL;
        MAX_WAIT_TIME = UPDATE_INTERVAL;
        Log.e("LocationModule", "UPDATE_INTERVAL " + UPDATE_INTERVAL + " FASTEST_UPDATE_INTERVAL: " + FASTEST_UPDATE_INTERVAL + " MAX_WAIT_TIME: " + MAX_WAIT_TIME);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getBaseContext());
        createLocationRequest();
        mActivityRecognitionClient = new ActivityRecognitionClient(getBaseContext());
        startLocationHandler();
        MainApplication.sendEvent("Location service on created invoked");
        MainApplication.logTime(MainApplication.keyServiceCreated);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MainApplication.sendEvent("Location service on start command called");
        startLocationTracking();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(1, NotificationUtils.notification(getApplicationContext()));
            MainApplication.sendEvent("Location service - foreground service started");
        } else {
            NotificationUtils.showNormalNotification(getApplicationContext());
        }
        MainApplication.updateDeviceInfo();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        MainApplication.logTime(MainApplication.keyServiceDestroyed);
        manageLocationUpdate(false);
        MainApplication.sendEvent("Location service destroyed");
        if (null != locationHandler) {
            locationHandler.removeCallbacks(locationRunnable);
        }
        super.onDestroy();
    }

    @SuppressLint("MissingPermission")
    private void startLocationTracking() {
        manageLocationUpdate(true);
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(Priority.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }


    @SuppressLint("MissingPermission")
    private void manageLocationUpdate(final boolean isStart) {
        if (isStart) {
            Toast.makeText(getApplicationContext(), "Location Updates Started!", Toast.LENGTH_SHORT).show();
            MainApplication.sendEvent("Location service - Location update started");
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, NotificationUtils.getPendingIntentBroadcast(getApplicationContext()));
            Hyperzod.getPreferenceUtil().setValue("RequestingLocationUpdates", true);

            Task<Void> task = mActivityRecognitionClient.requestActivityUpdates(
                    UPDATE_INTERVAL,
                    NotificationUtils.getPendingIntentBroadcast(getApplicationContext()));

            task.addOnSuccessListener(result -> {
                MainApplication.sendEvent("Location service - Task start success");

            });
            task.addOnFailureListener(e -> {
                MainApplication.sendEvent("Location service - Task start failure: " + e.getMessage());
            });

        } else {
            Hyperzod.getPreferenceUtil().setValue("RequestingLocationUpdates", false);
            MainApplication.sendEvent("Location service - Location update stopped");
            mFusedLocationClient.removeLocationUpdates(NotificationUtils.getPendingIntentBroadcast(getApplicationContext()));
            Utils.removeNotification(getApplicationContext());

            Toast.makeText(getApplicationContext(), "Location Updates Stopped!", Toast.LENGTH_SHORT).show();

            Task<Void> task = mActivityRecognitionClient.removeActivityUpdates(
                    NotificationUtils.getPendingIntentBroadcast(getApplicationContext()));
            task.addOnSuccessListener(result -> {
                MainApplication.sendEvent("Location service - Task stop success");
            });
            task.addOnFailureListener(e -> {
                MainApplication.sendEvent("Location service - Task stop failure: " + e.getMessage());
            });
        }
    }

    private void startLocationHandler() {
        if (null == locationHandler) {
            locationHandler = new Handler(Looper.getMainLooper());
        }
        locationHandler.postDelayed(locationRunnable, 60000);
    }

    private Runnable locationRunnable = () -> {
        if (!isLocationUpdating()) {
            MainApplication.sendEvent("Location service restarted");
            MainApplication.logTime(MainApplication.keyServiceRestarted);
            startLocationTracking();
        }
        startLocationHandler();
        MainApplication.logTime(MainApplication.keyHeartBeat);
    };

    private boolean isLocationUpdating() {
        try {
            long differenceInTime = Calendar.getInstance().getTime().getTime() - LocationRepository.lastUpdatedTime.getTime().getTime();
            long differenceInMinutes = (differenceInTime / (1000 * 60)) % 60;
            return differenceInMinutes < 2;
        } catch (Exception e) {
        }
        return true;
    }

}