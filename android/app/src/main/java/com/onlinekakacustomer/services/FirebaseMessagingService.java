package com.onlinekakacustomer.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import com.onlinekakacustomer.helper.NotificationUtils;

import java.io.File;


public class FirebaseMessagingService extends com.dieam.reactnativepushnotification.modules.RNPushNotificationListenerService {

    private final String soundDirSystem = "system_sounds/notif-sounds";
    private final String soundDirCustom = "custom_sounds/notif-sounds";
    private Ringtone ringtone;
    public static MediaPlayer mediaPlayer;
    private Uri rawPathUri;
    private boolean repeatSound = false;
    private static int streamVolume;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("FirebaseTest", "onNewToken: " + token);
    }

    @Override
    public void onMessageReceived(com.google.firebase.messaging.RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        boolean cancelSound = false;
        remoteMessage.getData();
        repeatSound = remoteMessage.getData().containsKey("repeat_sound") && Boolean.parseBoolean(remoteMessage.getData().get("repeat_sound"));
        Log.e("FirebaseTest", "vibration : " + remoteMessage.getData().get("device_vibration"));
        if (remoteMessage.getData().containsKey("sound")) {
            String sound = remoteMessage.getData().get("sound");
            String systemSoundPath = getApplicationContext().getFilesDir().getAbsolutePath() + "/" + soundDirSystem + "/" + sound;
            String customSoundPath = getApplicationContext().getFilesDir().getAbsolutePath() + "/" + soundDirCustom + "/" + sound;
            Log.e("NotifSound", "systemSoundPath: " + systemSoundPath);
            Log.e("NotifSound", "customSoundPath: " + customSoundPath);
            File fileSystem = new File(systemSoundPath);
            File fileCustom = new File(customSoundPath);
            Log.e("NotifSound", "fileSystem: " + fileSystem.getAbsolutePath() + " " + fileSystem.exists());
            Log.e("NotifSound", "fileCustom: " + fileCustom.getAbsolutePath() + " " + fileCustom.exists());
            cancelSound = fileSystem.exists() || fileCustom.exists();
//            if (fileSystem.exists()) {
//                rawPathUri = Uri.fromFile(fileSystem);
//                ringtone = RingtoneManager.getRingtone(getApplicationContext(), rawPathUri);
//                ringtone.play();
//            } else if (fileCustom.exists()) {
//                rawPathUri = Uri.fromFile(fileCustom);
//                ringtone = RingtoneManager.getRingtone(getApplicationContext(), rawPathUri);
//                ringtone.play();
//            }
            stopSound(this);
            if (fileSystem.exists()) {
                rawPathUri = Uri.fromFile(fileSystem);
                playSound();
            } else if (fileCustom.exists()) {
                rawPathUri = Uri.fromFile(fileCustom);
                playSound();
            }

        }

        if (!cancelSound) {
            rawPathUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            playSound();
        }

        String title = "";
        String body = "";
        if (remoteMessage.getData().containsKey("title")) {
            title = remoteMessage.getData().get("title");
        }
        if (remoteMessage.getData().containsKey("body")) {
            body = remoteMessage.getData().get("body");
        }

        NotificationUtils.showNormalNotification(getApplicationContext(), title, body, cancelSound, rawPathUri);
        if (remoteMessage.getData().containsKey("device_vibration") && (Boolean.parseBoolean(remoteMessage.getData().get("device_vibration")))) {
            vibrate();
        }
        Log.e("FirebaseTest", "FirebaseMessagingService: " + remoteMessage.getData().toString());
    }

    @SuppressLint("MissingPermission")
    private void vibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            long[] timings = {0, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500, 200, 500};
            int[] amplitudes = {0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180, 0, 180};
            v.vibrate(VibrationEffect.createWaveform(timings, amplitudes, -1));
        } else {
            v.vibrate(500);
        }
    }

    public static void stopSound(Context context) {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
//            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, streamVolume, 0);
            mediaPlayer.stop();
        }
    }

//     private void playSound() {
// //        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
// //        streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
// //        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
//         mediaPlayer = MediaPlayer.create(getApplicationContext(), rawPathUri);
//         mediaPlayer.setLooping(repeatSound);
//         mediaPlayer.start();
//     }
private void playSound() {
    try {
        if (mediaPlayer != null) {
            mediaPlayer.release(); // Release any previous instance of mediaPlayer.
        }
        
        mediaPlayer = MediaPlayer.create(getApplicationContext(), rawPathUri);

        if (mediaPlayer != null) {
            mediaPlayer.setLooping(repeatSound);
            mediaPlayer.start();
        } 
    } catch (Exception e) {
        e.printStackTrace();
        // Handle any exceptions that may occur during media playback.
    }
}
}
