#!/bin/sh
#
# Copyright (C) 2018 daisuke-t
#
# Make android icon(ic_launcher.png) on Mac OS.
#
# Arguments:
#   $1 - source image
#
# Note:
#   Source image is PNG 24 format.


# check args.
if [ $# -ne 3 ]; then
	echo "invalid argument."
	echo $#
	exit
fi

SRC_FILE=$1
DEST_FOLDER=$2
ICON_TYPE=$3


# prepare build folder.
# rm -rf build
# mkdir build
# mkdir build/mipmap-mdpi
# mkdir build/mipmap-hdpi
# mkdir build/mipmap-xhdpi
# mkdir build/mipmap-xxhdpi
# mkdir build/mipmap-xxxhdpi


# make square icon.
if [ $ICON_TYPE -eq 0 ]; then
sips --resampleWidth 48 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-mdpi/ic_launcher.png
sips --resampleWidth 72 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-hdpi/ic_launcher.png
sips --resampleWidth 96 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xhdpi/ic_launcher.png
sips --resampleWidth 144 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xxhdpi/ic_launcher.png
sips --resampleWidth 192 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xxxhdpi/ic_launcher.png
fi

# make rounded icon.
if [ $ICON_TYPE -eq 1 ]; then
sips --resampleWidth 48 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-mdpi/ic_launcher_round.png
sips --resampleWidth 72 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-hdpi/ic_launcher_round.png
sips --resampleWidth 96 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xhdpi/ic_launcher_round.png
sips --resampleWidth 144 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xxhdpi/ic_launcher_round.png
sips --resampleWidth 192 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xxxhdpi/ic_launcher_round.png
fi

# make Foreground icon.
if [ $ICON_TYPE -eq 2 ]; then
sips --resampleWidth 108 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-mdpi/ic_launcher_foreground.png
sips --resampleWidth 162 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-hdpi/ic_launcher_foreground.png
sips --resampleWidth 216 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xhdpi/ic_launcher_foreground.png
sips --resampleWidth 324 ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xxhdpi/ic_launcher_foreground.png
sips --resampleWidth 432s ${SRC_FILE} --out ${DEST_FOLDER}/mipmap-xxxhdpi/ic_launcher_foreground.png
fi