/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {Component} from 'react';
import LottieView from 'lottie-react-native';

export default class Splash extends Component {
  render() {
    return (
      <LottieView
        style={{
          // flex: 1,
          // height: 100,
          // width: 100,
          padding: 0,
          margin: 0,
          aspectRatio:1,
        }}
        resizeMode="cover"
        source={require('./animation.json')}
        autoPlay
        loop={false}
        // onAnimationFinish={() => this.props.navigation.replace('Home')}
      />
    );
  }
}
