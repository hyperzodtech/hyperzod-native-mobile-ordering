/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import Home from './Home';
import Splash from './Splash';
import Splash from './CommonHelper/HelpWebView';
//Import react-navigation
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import HelpWebView from './CommonHelper/HelpWebView';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerShown:false
      },
    },
    Splash: {
      screen: Splash,
      navigationOptions: {
        headerShown:false
      },
    },
    HelpWebView: {
      screen: HelpWebView,
      navigationOptions: {
        headerShown:false
      },
    }
  },
  {
    initialRouteName: 'Splash',
  },
);

const AppContainer = createAppContainer(RootStack);
// Now AppContainer is the main component for React to render
export default AppContainer;