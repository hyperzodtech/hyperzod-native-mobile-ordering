//
//  LocationModule.swift
//  OnlinekakaCustomer
//
//  Created by krunal on 27/08/22.
//

import Foundation
import CoreLocation

@objc(LocationModule)
class LocationModule: NSObject,CLLocationManagerDelegate {
  let locationManager = CLLocationManager()
  var currentLocation:CLLocationCoordinate2D?
  var timer = Timer()
  var mainDict = NSDictionary()
  var payloadDict = NSDictionary()
  
  @objc
  func showAlert(_ dict:NSDictionary,withPayload payload:NSDictionary) {
    print("showAlert")
    DispatchQueue.main.async {
      let appDel = UIApplication.shared.delegate as! AppDelegate
      let alert = UIAlertController(title: "Success", message: "Welcome \(dict) \(payload)", preferredStyle: .alert)
      let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
      }
      alert.addAction(okAction)
      appDel.window.rootViewController!.present(alert, animated: true, completion: nil)
    }
  }
  
  @objc
  func startService(_ dict:NSDictionary,withPayload payload:NSDictionary) {
    
    self.locationManager.requestAlwaysAuthorization()
    self.locationManager.requestWhenInUseAuthorization()
    print("startService")
    
    if CLLocationManager.locationServicesEnabled() {
      locationManager.delegate = self
      locationManager.allowsBackgroundLocationUpdates = true
      locationManager.pausesLocationUpdatesAutomatically = false
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager.distanceFilter = 0
      locationManager.startUpdatingLocation()
      print("Start...\(dict)")
      //      if let intervalVal = dict["agent_location_update_interval_seconds"] as? Int {
      //      Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: { timer in
      //        print("Timer call")
      //        //                self.locationApiCall(dict, withPayload: payload, isOnDuty: true)
      //      })
      //      }
      //        self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.locationApiCall(_:withPayload:isOnDuty:)), userInfo: nil, repeats: true)
      
      //      self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.locationApiCall(_:withPayload:isOnDuty:)), userInfo: nil, repeats: true)
      
      //      let timer = Timer.scheduledTimer(timeInterval: 2,
      //                                          target: self,
      //                                          selector: #selector(handleTimerExecution),
      //                                          userInfo: nil,
      //                                          repeats: true)
      self.mainDict = dict
      self.payloadDict = payload
      locationApiCall(dict, withPayload: payload, isOnDuty: true)
      
    }
  }
  
  @objc private func handleTimerExecution() {
    print("timer executed...")
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
    //    let deviceId = UIDevice.current.identifierForVendor?.uuidString  ?? ""
    //
    //    let date = Date()
    //    let dateFormatter = DateFormatter()
    //    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    //    var sid = dateFormatter.string(from: date)
    
    //      let locationDict = ["lat":"\(locValue.latitude)","long":"\(locValue.longitude)","device":"iOS","time":FieldValue.serverTimestamp()] as [String : Any];
    //      db.collection("userLocationTests").document(deviceId).setData(locationDict)
    currentLocation = locValue
    print("locations = \(locValue.latitude) \(locValue.longitude)")
    locationApiCall(mainDict, withPayload: payloadDict, isOnDuty: true)
  }
  
  @objc
  func stopService(_ dict:NSDictionary,withPayload payload:NSDictionary) {
    locationManager.stopUpdatingLocation()
    locationApiCall(dict, withPayload: payload, isOnDuty: false)
  }
  
  @objc func locationApiCall(_ dict:NSDictionary,withPayload payload:NSDictionary,isOnDuty dutyFlag:Bool){
    
    //    LOG  post message {"agent_location_update_api_url": "https://www.autozod.dev/api/v1/agent/updateLocation", "agent_location_update_interval_seconds": 10, "postMessageType": "DriverAppBootSettings"}
    
    //    LOG  AgentLoggedIn ->  {"agent_id": "613754be5584cb0a7e539c24", "postMessageType": "AgentLoggedIn", "team_id": 16, "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5hdXRvem9kLmRldi9hcGkvdjEvYWdlbnQvbG9naW4iLCJpYXQiOjE2NjE1OTU0MTMsImV4cCI6MTY2NDAxNDYxMywibmJmIjoxNjYxNTk1NDEzLCJqdGkiOiIzTUhialpHNWpPY0xsSkpsIiwic3ViIjoiNjEzNzU0YmU1NTg0Y2IwYTdlNTM5YzI0IiwicHJ2IjoiNDJlZGUzMzM0YTA0Y2Q2NWNiMmJlM2Y1YWRmZWIyYzFlZGQzMDc3NCJ9.nhm2vI6SLJXP5WOe1M4Iquj1vbFLbIWAiZ1ujKopfss"}
    
    if payload.allKeys.count == 0 {
      return
    }
    
    print("call api method")
    let payloadData : [String:Any] = [
      "team_id": payload["team_id"] as! Int,
      "agent_id": payload["agent_id"] as! String,
      "on_duty":dutyFlag,
      "location":[
        (currentLocation != nil) ? "\(currentLocation!.longitude)" : "0.0",
        (currentLocation != nil) ? "\(currentLocation!.latitude)" : "0.0"
      ]
    ]
    print(payloadData)
    guard let locationApi = dict["agent_location_update_api_url"] as? String else {
      return
    }
    
    do{
      let postData = try JSONSerialization.data(withJSONObject: payloadData, options: [])
      
      var urlReq = URLRequest(url: URL(string: locationApi)!)
      urlReq.httpMethod = "POST"
      urlReq.httpBody = postData
      urlReq.setValue("application/json", forHTTPHeaderField: "Content-Type")
      let token = "Bearer \(payload["token"] as! String)"
      urlReq.setValue(token, forHTTPHeaderField: "Authorization")
      
      let task = URLSession.shared.dataTask(with: urlReq) { (data, response, error) in
        if let error = error {
          // Handle HTTP request error
        } else if let data = data {
          // Handle HTTP request response
          do
          {
            let dict = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
            print("Response \(dict)")
          }
          catch{
          }
        } else {
          // Handle unexpected error
        }
      }
      task.resume()
    }catch{
    }
  }
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
}
