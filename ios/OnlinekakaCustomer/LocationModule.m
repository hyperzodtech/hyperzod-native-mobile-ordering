//
//  LocationModule.m
//  OnlinekakaCustomer
//
//  Created by krunal on 27/08/22.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
@interface
RCT_EXTERN_MODULE(LocationModule, NSObject)
RCT_EXTERN_METHOD(showAlert:(NSDictionary *)dict withPayload:(NSDictionary *)payload)
RCT_EXTERN_METHOD(startService:(NSDictionary *)dict withPayload:(NSDictionary *)payload)
RCT_EXTERN_METHOD(stopService:(NSDictionary *)dict withPayload:(NSDictionary *)payload)
@end
