# OnlineKaka
RN Project for managing Customer & Merchant App.

# Prerequisite for running app
npm install -g react-native-cli
npm install -g create-react-native-app

# Running on Android
export ANDROID_HOME=~/Library/Android/sdk    
npx react-native run-android --variant=merchantDebug
npx react-native run-android --variant=driverDebug
npx react-native run-android --variant=customerDebug


Make apk:
cd android && ./gradlew assembleRelease
# Steps to run app on iOS
- npm install
- cd ios && pod install && cd ..
- npx react-native run-ios
- npx react-native run-ios --simulator="iPhone 8"
# Steps to run app on Android
- npm install
- npx react-native run-android

# Steps to make release apk
- cd android && ./gradlew assembleRelease

Install NodeJS Plugin on jenkins also
set this jenkins->manage plugin -> Global 

export ANDROID_HOME=~/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

# iOS Script
 #!/bin/bash -l

export CUSTOM_PATH="/Users/krunal/GoogleDrive/Apps-Build/Customer-APP"

npm install
cd ios && pod install

chmod u+x ios-icon-generator.sh

./ios-icon-generator.sh "${CUSTOM_PATH}/${COMPANY_NAME}/ios-icon.png" "${WORKSPACE}/ios/OnlinekakaRNCustomer/Images.xcassets/AppIcon.appiconset"

/usr/libexec/PlistBuddy -c "Set :CFBundleDisplayName $APP_NAME" "OnlinekakaRNCustomer/Info.plist"
echo "☛ APP NAME: ${APP_NAME}"

/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $VERSION_NUMBER" "OnlinekakaRNCustomer/Info.plist"
echo "☛ VERSION NUMBER: ${VERSION_NUMBER}"

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $BUILD_NUMBER" "OnlinekakaRNCustomer/Info.plist"
echo "☛ BUILD NUMBER: ${BUILD_NUMBER}"

xcodebuild -workspace OnlinekakaRNCustomer.xcworkspace -scheme OnlinekakaRNCustomer -configuration Release -archivePath build/OnlineKakaCustomer archive
xcodebuild -exportArchive -archivePath build/OnlineKakaCustomer.xcarchive -exportPath ipa-release/ -exportOptionsPlist RunnerUpload.plist
xcrun altool --upload-app --type ios --file "ipa-release/OnlinekakaRNCustomer.ipa" --username ios@hyperzod.com --password iivl-sjjz-siay-okpq



# Android Native Script

#!/bin/bash -l

export ANDROID_SDK_ROOT=~/Library/Android/sdk
export CUSTOM_PATH="/Users/hyperzod/Desktop/Apps-Build/Customer-APP"

echo "☛ Android"

./gradlew clean

if [ -f "${WORKSPACE}/app/build" ]; then
    echo "Delete Old Apks"
	rm -d "${WORKSPACE}/app/build"
fi

cp -f "${CUSTOM_PATH}/${COMPANY_NAME}/Configuration.json" "${WORKSPACE}/app/src/main/assets/Configuration.json"


sed -i "" "s/OnlineKaka Customer/$APP_NAME/g" "${WORKSPACE}/app/build.gradle"
echo "☛ App Name update"

sed -i "" "s/com.customer.dev/$BUNDLE_ID/g" "${WORKSPACE}/app/build.gradle"
echo "☛ BundleID update"



cp -f "${CUSTOM_PATH}/${COMPANY_NAME}/android-logo.png" "${WORKSPACE}/app/src/main/res/drawable/app_logo.png"
echo "☛ logo File copied"


cp -f "${CUSTOM_PATH}/${COMPANY_NAME}/google-services.json" "${WORKSPACE}/app/google-services.json"
echo "☛ google-services.json File copied"

if [ -f "${CUSTOM_PATH}/${COMPANY_NAME}/animation.json" ]; then
    echo "Copy Animation file"
	cp -f "${CUSTOM_PATH}/${COMPANY_NAME}/animation.json" "${WORKSPACE}/app/src/main/res/raw/splash.json"
fi


if [ -f "${CUSTOM_PATH}/${COMPANY_NAME}/splash_back.png" ]; then
    echo "Copy Background Image"
	cp -f "${CUSTOM_PATH}/${COMPANY_NAME}/splash_back.png" "${WORKSPACE}/app/src/main/res/drawable/splash_back.png"
fi


# Lottie // Splash Image
    to change lottie / splash image pass file as txt (json inside)/ jpg in settings api 
    else default animation is loaded from ./animation.json



echo "☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛KeyStore Check starts here based on AppId☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛"

KEY_PWD="admin123!"
KEY_ALIAS="KHAPACHI"
KEY_FILE="keystore.jks"


if [ $BUNDLE_ID == "com.customer.a2bdelivery" ]
then
echo "A2BDelivery Keystore"
KEY_PWD="a2b-delivery@123"
KEY_ALIAS="a2b-delivery"
KEY_FILE="a2bdelivery.jks"

elif [ $BUNDLE_ID == "com.azloonline.android" ]
then
echo "AzloOnline Keystore"
KEY_PWD="azlo@123"
KEY_ALIAS="azlo"
KEY_FILE="azloonline.jks"

elif [ $BUNDLE_ID == "com.customer.blackeats" ]
then
echo "BlackEats Keystore"
KEY_PWD="blackeats@123"
KEY_ALIAS="blackeats"
KEY_FILE="blackeats.jks"

elif [ $BUNDLE_ID == "com.growinlocal.android" ]
then
echo "GrowInLocal Keystore"
KEY_PWD="admin123!"
KEY_ALIAS="KHAPACHI"
KEY_FILE="growinlocal.jks"

elif [ $BUNDLE_ID == "com.customer.homefoods" ]
then
echo "HomeFood Keystore"
KEY_PWD="homefoods@123"
KEY_ALIAS="homefoods"
KEY_FILE="homefoods.jks"


elif [ $BUNDLE_ID == "com.wonderpillars.onlinekaka" ]
then
echo "OnlineKaka Keystore"
KEY_PWD="onlinekaka@321"
KEY_ALIAS="OnlineKaka"
KEY_FILE="onlinekaka.jks"

elif [ $BUNDLE_ID == "com.wezz.customer" ]
then
echo "WeZz Keystore"
KEY_PWD="wezz@123"
KEY_ALIAS="wezz"
KEY_FILE="wezz.jks"

elif [ $BUNDLE_ID == "com.influx.groccartwebview" ]
then
echo "GrocCart Keystore"
KEY_PWD="Influex@331"
KEY_ALIAS="key0"
KEY_FILE="groccart.jks"

else
echo "COMMON Keystore"
fi

sed -i "" "s/keystore.jks/$KEY_FILE/g" "${WORKSPACE}/app/build.gradle"
sed -i "" "s/admin123!/$KEY_PWD/g" "${WORKSPACE}/app/build.gradle"
sed -i "" "s/KHAPACHI/$KEY_ALIAS/g" "${WORKSPACE}/app/build.gradle"




echo "☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛KeyStore Check ends here☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛"

sed -i "" "s/AIzaSyBik4dEFibTn9Qw-KnQ9t-dAaz48fm_ma4/$GOOGLE_KEY/g" "${WORKSPACE}/app/build.gradle"

echo "☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛ MAP API KEY ☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛"


sed -i "" "s/306586567869744/$FB_App_ID/g" "${WORKSPACE}/app/build.gradle"

echo "☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛ FB_App_ID Updated ☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛"



./make_android_icon.sh "${CUSTOM_PATH}/${COMPANY_NAME}/android-icon.png" "${WORKSPACE}/app/src/main/res" 0
./make_android_icon.sh "${CUSTOM_PATH}/${COMPANY_NAME}/android-icon.png" "${WORKSPACE}/app/src/main/res" 1
./make_android_icon.sh "${CUSTOM_PATH}/${COMPANY_NAME}/android-icon.png" "${WORKSPACE}/app/src/main/res" 2


chmod +x gradlew && ./gradlew clean && ./gradlew -PversCode=${BUILD_NUMBER} -PversName=${VERSION_NUMBER} assembleonlinekakaRelease

if [ -f "${WORKSPACE}/app/build/outputs/apk/onlinekaka/release/spledy_devloper-onlinekaka-release.apk" ]; then
    mv "${WORKSPACE}/app/build/outputs/apk/onlinekaka/release/spledy_devloper-onlinekaka-release.apk" "${WORKSPACE}/app/build/outputs/apk/onlinekaka/release/${COMPANY_NAME}-CustomerApp.apk"
    cp -f "${WORKSPACE}/app/build/outputs/apk/onlinekaka/release/${COMPANY_NAME}-CustomerApp.apk" "${CUSTOM_PATH}/${COMPANY_NAME}"
fi

# echo "☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛Make PlayStore app☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛☛"
#./gradlew -PversCode=${BUILD_NUMBER} -PversName=${VERSION_NUMBER} bundleonlinekakaRelease

#if [ -f "${WORKSPACE}/app/build/outputs/bundle/onlinekakaRelease/spledy_devloper-onlinekaka-release.aab" ]; then
#	mv "${WORKSPACE}/app/build/outputs/bundle/onlinekakaRelease/spledy_devloper-onlinekaka-release.aab" "${WORKSPACE}/app/build/outputs/bundle/onlinekakaRelease/${COMPANY_NAME}-CustomerApp.aab"
#	cp -f "${WORKSPACE}/app/build/outputs/bundle/onlinekakaRelease/${COMPANY_NAME}-CustomerApp.aab" "${CUSTOM_PATH}/${COMPANY_NAME}"
#fi

# Run android debug mode
export ANDROID_HOME=~/Library/Android/sdk                         
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

# CodeMagic Signing
https://docs.codemagic.io/yaml-code-signing/code-signing-personal/