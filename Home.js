import React from 'react';
import {
  Image,
  Platform,
  BackHandler,
  Dimensions,
  Modal,
  View,
  Text,
  StatusBar,
  Linking,
  Alert,
  PermissionsAndroid,
  Vibration,
  KeyboardAvoidingView,
  Pressable,
  Button,
  DeviceEventEmitter,
} from 'react-native';
import {Component} from 'react';
import {WebView} from 'react-native-webview';
import LottieView from 'lottie-react-native';
import RNFS from 'react-native-fs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PushNotification, {
  PushNotificationObject,
} from 'react-native-push-notification';
import messaging, {
  FirebaseMessagingTypes,
} from '@react-native-firebase/messaging';
import NotifService from './NotifService';
import {InAppBrowser} from 'react-native-inappbrowser-reborn';
import DeviceDetails from './DeviceDetails';
import Geolocation from 'react-native-geolocation-service';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {getStatusBarHeight} from 'react-native-status-bar-height';
const {height, width} = Dimensions.get('window');
import {SvgXml} from 'react-native-svg';
import RNRestart from 'react-native-restart';
import {AppState} from 'react-native';
import {NativeEventEmitter, NativeModules} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import {unzip} from 'react-native-zip-archive';
import animationJson from './animation.json';
import splashVideo from './splashVideo.mp4';
import crashlytics from '@react-native-firebase/crashlytics';
import * as Sentry from '@sentry/react-native';
import Video from 'react-native-video';
import AppShare from './AppShare';

const {config, fs} = RNFetchBlob;
const {LocationModule, PrinterModule} = NativeModules;

const eventEmitter = new NativeEventEmitter(NativeModules.LocationModule);

// const noNetworkErrorCode = -1009;
// const noNetworkErrorMessage = "ERR_INTERNET_DISCONNECTED";
// const domainErrorCode = -1003;
// const domainErrorMessage = "ERR_NAME_NOT_RESOLVED";

export default class Home extends Component {
  webView = {
    canGoBack: false,
    ref: null,
  };
  driverBootSettings = {};
  driverDetailPayload = {};
  isWebviewLoaded = false;
  callSplashScriptDone = false;
  statusBarHeight = 0;
  isDriverLoggedIn = false;
  isInappBrowserClosed = null;
  // isAppLoaded = false;
  // isLocationDialogShowwed = false;
  // isLocationPermissionDialogShowwed = false;
  splashAnimation = {
    ref: null,
  };

  constructor(props) {
    super(props);
    this.appShareRef = React.createRef();
    this.state = {
      load: true,
      i: 0,
      fileJson: {},
      fileMp4Video: '',
      splashDuration: 4,
      fileImage: '',
      existImage: false,
      existsJson: false,
      existsMp4Video: false,
      canGoBack: false,
      showLocalAnimation: false,
      showLocalImage: false,
      showLocalMp4Video: false,
      payload: '',
      webViewError: false,
      isStatusDenied: false,
      errorDescription: '',
      errorMessage: '',
      allowApplePay: false,
      bgColor: '#00FF00',
      appUrl: 'https://www.arything.com/',
      // appUrl:
      //   'https://redirect.hyperzod.dev/1003/native_admin?role=T1003&saveRole=true',
      // appUrl: 'https://autozod-agent-client.netlify.app/?team_id=4',
      // https://app-agent.autozod.app?team_id= 1018
      notificationData: '',
      statusbarColor: '#0000FF',
      statusContent: 'light',
      webviewKey: 0,
      lastInactiveTime: 0,
      app_state: null,
      subscription: null,
      clientAppSchemeName: 'hyperzodscheme',
      isBGLocationPermissionEnabled: false,
      isBatteryRestiction: false,
    };
    this.handleAppEvents = this.handleAppEvents.bind(this);
    this.handleLocationPermissionEvents =
      this.handleLocationPermissionEvents.bind(this);
    //Customer Test link
    // https://www.arything.com/

    //Driver Test link
    //https://autozod-agent-client.netlify.app/

    //Merchant Test link
    //https://redirect.hyperzod.dev/1003/native_admin?role=T1003&saveRole=true

    Sentry.init({
      dsn: 'https://17672df1145a7ff3d4f7832b79bb9d5b@o1008315.ingest.sentry.io/4506189228474368',
      environment: 'production',
      tracesSampleRate: 0.2,
    });

    this.checkExistFile();
    AsyncStorage.getItem('configuration').then(responseVal => {
      if (responseVal != null) {
        let response = JSON.parse(responseVal);
        this.setState({
          bgColor: response.background_color,
          splashDuration: parseInt(response.splash_duration),
          statusbarColor: response.status_bar_bg_color,
          statusContent: response.status_bar_content_style,
        });
      }

      setTimeout(() => {
        console.log(
          'Splash check >>>> timer completed ' +
            this.state.splashDuration +
            ' this.isWebviewLoaded: ' +
            this.isWebviewLoaded,
        );
        this.callSplashScriptDone = true;
        // if(this.isAppLoaded==true){
        //   // alert('hii')
        //   this.setState({
        //     load: false
        //   });
        // }
        if (Platform.OS === 'ios') {
          const insets = StaticSafeAreaInsets.safeAreaInsetsTop;
          console.log('insets->> ', insets);
          if (insets > 0) {
            this.statusBarHeight = insets;
            console.log('statusBarHeight', this.statusBarHeight);
          } else {
            this.statusBarHeight = getStatusBarHeight(true);
          }
          // this.webView.ref.injectJavaScript('window.setMobileBottomNavPadding(' + bottomPadding + ')');
        } else {
          //for android set statusBarHeight
          this.statusBarHeight = getStatusBarHeight(true);
        }

        if (this.isWebviewLoaded == true) {
          console.log('Splash check >>>> set state called timer');
          this.setState({
            load: false,
          });
          // if (this.splashAnimation.ref != null) {
          //   this.splashAnimation.ref.reset();
          // }
        }
      }, this.state.splashDuration * 1000);
    });

    // Notifications.init(this.handleRegister);
    //  this.readFile()
    // this.pushNotificationListner();

    messaging().onMessage(async remoteMessage => {
      console.log('Message handled in the foreground!', remoteMessage);
      // Creating Local notification
      this.showNotification(
        remoteMessage.notification?.title ?? '',
        remoteMessage.notification.body ?? '',
        remoteMessage.data ?? '',
      );
    });

    // Register background handler
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });

    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        // do whatever you want to here
        console.log('Message handled in the Quit!', remoteMessage);
        var data =
          JSON.stringify(remoteMessage.data).length == 2
            ? 'No data'
            : JSON.stringify(remoteMessage.data);
        // this.deeplinkAgain(data);
        this.setState({
          notificationData: data,
        });
      })
      .catch(err => {});

    messaging().onNotificationOpenedApp(async remoteMessage => {
      console.log(
        'Message handled on click!',
        JSON.stringify(remoteMessage.data).length,
      );
      var data =
        JSON.stringify(remoteMessage.data).length == 2
          ? 'No data'
          : JSON.stringify(remoteMessage.data);
      this.deeplinkNotification(data);
    });
    //this.createChannel();
    this.notif = new NotifService(
      this.onRegister.bind(this),
      this.onNotif.bind(this),
    );
    this.notif.createOrUpdateChannel();
    this.onAppStateChange = nextAppState => {
      console.log('App State Change ' + nextAppState);
      if (nextAppState === 'background') {
        var currentTimeInSeconds = Math.floor(Date.now() / 1000);
        this.lastInactiveTime = currentTimeInSeconds;
        console.log('BGGG ' + this.lastInactiveTime);
      } else if (nextAppState === 'active') {
        console.log('App State Change to: ' + nextAppState);
        if (Platform.OS == 'ios' && this.isDriverLoggedIn) {
          this.getDeviceCurrentLocationNew();
        }
        if (Platform.OS == 'android' && this.isDriverLoggedIn) {
          this.checkLocationPermissionAndroid();
          this.checkBatteryOptimizationEnabled();
          this.checkPhysicalActivityEnabled();
        }
        var currentTimeInSeconds = Math.floor(Date.now() / 1000);
        var diff = currentTimeInSeconds - this.lastInactiveTime;
        console.log('BGGG ' + diff);
        // if (this.splashAnimation.ref != null) {
        //   this.splashAnimation.ref.reset();
        // }
        if (
          diff >
          3600 /*|| this.webView.ref === null || this.webView.ref === undefined*/
        ) {
          this.reloadApp();
        }
      }
    };
  }

  handleAppEvents(event) {
    console.log('locationDisabled : ' + event);
    if (event == false) {
      try {
        if (this.webView.ref) {
          console.log(
            'isLocationDialogShowwed->>',
            this.isLocationDialogShowwed,
          );
          this.webView.ref.injectJavaScript(`window.showEnableLocationAlert()`);
        }
      } catch (e) {}
    }
  }

  handleLocationPermissionEvents(event) {
    // if user allowed the permission the event return false.
    console.log('locationPermissionAllowed->>> ', event);
    if (event == true) {
      try {
        if (this.webView.ref) {
          console.log('showAllowLocationPermissionAlert->>>');
          // console.log('LocationModule >>> isLocationPermission : ' + event + " >>>>   this.isLocationPermissionDialogShowwed >>> " + this.isLocationPermissionDialogShowwed);
          // if (!this.isLocationPermissionDialogShowwed) {
          //   console.log('LocationModule >>> isLocationPermission : ' + event+" >>>>   this.isLocationPermissionDialogShowwed >>> "+this.isLocationPermissionDialogShowwed);
          //   this.isLocationPermissionDialogShowwed = true;
          this.webView.ref.injectJavaScript(
            `window.showAllowLocationPermissionAlert()`,
          );

          // }
        }
      } catch (e) {}
    }
  }

  async getDeviceCurrentLocationNew() {
    // console.log(this.state.isAgentOnDuty ? "true agent" : "false agent")
    if (Platform.OS == 'ios') {
      const status = await Geolocation.requestAuthorization('whenInUse');
      if (status === 'denied') {
        // this.showLocationErrorAlert();
        if (this.webView.ref) {
          console.log('showAllowLocationPermissionAlert->>>');
          this.webView.ref.injectJavaScript(
            `window.showAllowLocationPermissionAlert()`,
          );
        }
      }
    }
  }

  // async getDeviceInformation() {
  //   LocationModule.getAppVersionInfo(versionInfo => {
  //     console.log('versionInfo:', versionInfo);
  //     let versioninfo = JSON.stringify({
  //       appBuildNumber: versionInfo.versionCode,
  //       appversionName: versionInfo.versionName,
  //     });
  //     this.webView.ref.injectJavaScript(
  //       'window.getDeviceInfo(' + versioninfo + ')',
  //     );
  //   });
  // }

  async checkLocationPermissionAndroid() {
    LocationModule.checkLocationPermission(isPermissionGranted => {
      console.log('Permission check:', isPermissionGranted);
      if (!isPermissionGranted) {
        // this.handleLocationPermissionEvents
        console.log(
          'checkLocationPermission isPermissionGranted >>>' +
            isPermissionGranted,
        );
      } else {
        this.checkBackgroundLocationEnabled();
      }
    });
  }

  showNotification(title, message, data) {
    console.log('data -> ' + JSON.stringify(data));
    var localNotification = {
      id: 0,
      title: title,
      message: message,
      data: data,
      playSound: true,
      soundName: data.sound != null ? data.sound : 'default',
    };

    Platform.OS == 'android' &&
      (localNotification = {
        ...localNotification,
        channelId: 'Hyperzod', // (required) channelId, if the channel doesn't exist, notification will not trigger.
      });
    PushNotification.localNotification(localNotification);
  }

  deeplinkNotification(msgData) {
    setTimeout(() => {
      // console.log("WEB REF ",this.webView.ref);
      if (this.webView.ref) {
        console.log('111 ', msgData);
        this.webView.ref.injectJavaScript(
          `window.processNativePushNotification(${msgData})`,
        );
      } else {
        //deeplinkNotification(msgData);
        console.log('222');
        this.deeplinkAgain(msgData);
      }
    }, 5000);
  }

  deeplinkAgain(msgData) {
    setTimeout(() => {
      // console.log("WEB REF ",this.webView.ref);
      console.log('Deep Link ', msgData);
      if (this.webView.ref) {
        this.webView.ref.injectJavaScript(
          `window.processNativePushNotification(${msgData})`,
        );
      } else {
        this.deeplinkAgain(msgData);
      }
    }, 5000);
  }

  async checkPermission() {
    console.log('checkpermission');
    const authStatus = await messaging().requestPermission();
    // const enabled =
    //   authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    //   authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    // if (enabled) {
    console.log('Authorization status:', authStatus);
    // }
  }

  async getToken() {
    const authStatus = await messaging().requestPermission();
    // let fcmToken = await AsyncStorage.getItem('fcmToken');
    // console.log('FCM Token ', fcmToken);
    // if (!fcmToken) {
    //generate firebase notification token on each request
    let fcmToken = await messaging().getToken();
    console.log('FCM Token ', fcmToken);

    const msg = JSON.stringify({
      token: fcmToken,
      platform: Platform.OS,
    });

    this.webView.ref.injectJavaScript(
      'window.updateDeviceTokenForNativePushNotification(' + msg + ')',
    );
    // if (fcmToken) {
    // user has a device token
    //   console.log('user has a device token ', fcmToken);
    //   await AsyncStorage.setItem('fcmToken', fcmToken);
    // }
    // } else {
    //   console.log('FCM empty ', fcmToken);

    //   this.webView.ref.injectJavaScript(
    //     'window.updateDeviceTokenForNativePushNotification("' + fcmToken + '")',
    //   );
    // }
  }
  async getSplashUpdateTimeFromStorage(data) {
    console.log('RESP1--> here');
    var response = data; //JSON.parse(data);
    console.log('RESP1--> ' + response);
    if (response != null) {
      console.log('RESP--> ' + response);
      var currentTimeInSeconds = Math.floor(Date.now() / 1000);
      var updatedAtTime =
        response.updated_at != null
          ? response.updated_at
          : currentTimeInSeconds;
      let updateTime = await AsyncStorage.getItem('updatedSplashAT'); // != null ? await AsyncStorage.getItem('updatedSplashAT') : null;

      console.log('stored time ' + updateTime);
      if (updateTime == null || updatedAtTime != updateTime) {
        console.log('updatead splash screen need to download');
        this.downloadFile(response, updatedAtTime);
      } else {
        console.log('updatead splash screen no need to download');
      }

      /******Merchant Sound file download here****/
      if (response.sounds != null) {
        var updatedAtSoundTime = response.sounds.updated_at;
        let updatedSoundTime = await AsyncStorage.getItem('updatedSoundAT'); // != null ? await AsyncStorage.getItem(JSON.parse('updatedSoundAT')) : null;

        if (
          updatedSoundTime == null ||
          updatedAtSoundTime != updatedSoundTime
        ) {
          if (response.sounds.custom_sounds_url != null) {
            //download custom sound zip here
            console.log('download custom sound');
            this.downloadSoundFiles(
              response.sounds.custom_sounds_url,
              'custom_sounds',
              updatedAtSoundTime,
            );
          }
          if (response.sounds.system_sounds_url != null) {
            //download system sound zip here
            console.log('download system sound');
            this.downloadSoundFiles(
              response.sounds.system_sounds_url,
              'system_sounds',
              updatedAtSoundTime,
            );
          }
        }
      }
      /*******************************************/
    }
  }

  downloadSoundFiles(urlPath, soundType, updatedSoundAT) {
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.LibraryDir : dirs.MainBundleDir + '/files';
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: `${soundType}.zip`,
      path: `${dirToSave}/${soundType}.zip`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'zip',
      },
      android: configfb,
    });
    var download = config(configOptions)
      .fetch('GET', urlPath)
      .then(res => {
        // Alert after successful downloading
        console.log('sound download ' + res.path());
        // alert('File Downloaded Successfully.');
        if (res != null && res.path() != null) {
          //unzip folder
          unzip(
            res.path(),
            Platform.OS == 'ios'
              ? `${dirToSave}/Sounds`
              : `${dirToSave}/${soundType}`,
            'UTF-8',
          )
            .then(path => {
              console.log(`unzip completed at ${path}`);
              if (Platform.OS == 'ios') {
                RNFS.readDir(`${dirToSave}/Sounds`).then(result => {
                  for (let i = 0; i < result.length; i++) {
                    // console.log(`PATH---> ${result[i].path}`);
                    if (result[i].isDirectory()) {
                      // console.log(`Its DIRRR`);
                      RNFS.readDir(`${result[i].path}`).then(nestedResult => {
                        for (let j = 0; j < nestedResult.length; j++) {
                          // console.log(`PATHX---> ${nestedResult[j].name}`);
                          RNFS.moveFile(
                            nestedResult[j].path,
                            `${dirToSave}/Sounds/${nestedResult[j].name}`,
                          )
                            .then(success => {
                              // console.log('file moved!');
                            })
                            .catch(err => {
                              // console.log("Error: " + err.message);
                            });
                          //    this.moveAll(nestedResult[j].path, `${dirToSave}/Sounds` + '/' + nestedResult[i].name);
                        }
                      });
                    }
                  }
                });
              }
              if (updatedSoundAT != null) {
                AsyncStorage.setItem(
                  'updatedSoundAT',
                  JSON.stringify(updatedSoundAT),
                ).then(result => {});
              }
            })
            .catch(error => {
              console.error(error);
            });
        }
      });
  }
  async checkExistFile() {
    // console.log(
    //   'path =-->',
    //   await RNFS.exists(RNFS.DocumentDirectoryPath + '/react-native.json'),
    // );
    console.log('checkExistFile');
    if (await RNFS.exists(RNFS.DocumentDirectoryPath + '/react-native.json')) {
      // RNFetchBlob.fs.readFile(RNFS.DocumentDirectoryPath + '/react-native.json','json')
      RNFS.readFile(RNFS.DocumentDirectoryPath + '/react-native.json') // On Android, use "RNFS.DocumentDirectoryPath" On ios MainBundlePath (MainBundlePath is not defined)
        .then(result => {
          this.setState({
            existsJson: true,
            fileJson: JSON.parse(result),
          });
        })
        .catch(err => {
          console.log('Errorcatch', err.message, err.code);
        });
    } else if (
      await RNFS.exists(RNFS.DocumentDirectoryPath + '/react-native.mp4')
    ) {
      console.log('MP4 video file exists');
      var filePath = RNFS.DocumentDirectoryPath + '/react-native.mp4';
      this.setState({
        existsMp4Video: true,
        fileMp4Video: filePath,
      });
    } else if (
      await RNFS.exists(RNFS.DocumentDirectoryPath + '/react-native.png')
    ) {
      console.log('Image file exist');
      var filePath = RNFS.DocumentDirectoryPath + '/react-native.png';
      this.setState({
        existImage: true,
        fileImage: filePath,
      });
    } else {
      if (JSON.stringify(animationJson).length > 13) {
        this.setState({
          showLocalAnimation: true,
        });
      } else if (
        Image.resolveAssetSource(require('./splash-icon.png')).height != 249
      ) {
        this.setState({
          showLocalImage: true,
        });
      } else {
        this.setState({
          showLocalMp4Video: true,
        });
      }
      // console.log('Length >>> '+JSON.stringify(animationJson).length);
    }
  }

  handleError = error => {
    // console.log('webview error', error.nativeEvent.description);
    // console.log('webview error code', error.nativeEvent);
    this.setState({
      webViewError: true,
      errorDescription: this.getErrorDescription(error.nativeEvent.code),
      errorMessage:
        error.nativeEvent.code + ' - ' + error.nativeEvent.description,
    });
  };

  // getErrorMessage(errorEvent){
  //   if(errorEvent.code == noNetworkErrorCode || errorEvent.description.includes(noNetworkErrorMessage)){
  //       return 'Unable to connect to web server';
  //   }else{
  //     return '';
  //   }
  // }

  getErrorDescription(errorEvent) {
    if (errorEvent == 404 || errorEvent == -2 || errorEvent == -1009) {
      return 'No Internet connection found. Check your connection or try again.';
    } else if (
      (errorEvent > 400 && errorEvent < 600) ||
      errorEvent == -1202 ||
      errorEvent == 4
    ) {
      return 'Unable to contact to web server';
    } else {
      return 'Something Went Wrong';
    }
  }

  onLoadEndChangeView = () => {
    if (this.isWebviewLoaded == false) {
      this.webView.ref.injectJavaScript('window.nativeRequestAssets()');
      this.isWebviewLoaded = true;
    }
    //added by azeem
    // if(this.isAppLoaded == false){
    //   this.webView.ref.injectJavaScript('window.nativeRequestAssets()');
    //   // this.isWebviewLoaded = true;
    // }
    // if (Platform.OS == 'android') {
    //   LocationModule.checkLocationPermission();
    //   LocationModule.checkLocationStatus();
    // }

    console.log(
      'Splash check >>>> webview load completed  this.callSplashScriptDone: ' +
        this.callSplashScriptDone,
    );

    if (this.callSplashScriptDone == true) {
      // setTimeout(() => {
      console.log('Splash check >>>> set state called load end');
      this.setState({
        load: false,
      });
      // if (this.splashAnimation.ref != null) {
      //   this.splashAnimation.ref.reset();
      // }
      // }, 2 * 1000);
    }

    this.webView.ref.injectJavaScript(
      `document.body.style.userSelect ='none';`,
    );
    this.webView.ref.injectJavaScript(
      `document.body.style.webkitUserSelect ='none';`,
    );
    this.webView.ref.injectJavaScript(
      `document.body.style.webkitTouchCallout ='none';`,
    );
    this.injectBottomPadding();
  };
  injectBottomPadding() {
    if (Platform.OS === 'ios') {
      var bottomPadding = StaticSafeAreaInsets.safeAreaInsetsBottom;
      console.log('bottom padding: >>>>>>> ' + bottomPadding);
      this.webView.ref.injectJavaScript(
        'window.setMobileBottomNavPadding(' + bottomPadding + ')',
      );
    }
  }
  onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack,
    });
    console.log('nav state', navState.url);
    if (navState.url.includes('checkout')) {
      console.log('checkout EXIST');
      this.setState({
        allowApplePay: true,
      });
    } else {
      console.log('checkout NOT EXIST');
      this.setState({
        allowApplePay: false,
      });
    }

    /*if (navState.url.includes(this.state.appUrl)) {
      console.log('include app url');
      this.setState({
        lastAppUrl: navState.url,
      });
    }*/
    // if (Platform.OS === 'android') {
    // this.webView.ref.injectJavaScript(
    //   'window.setNativeStatusBarHeight(' +
    //     100 +
    //     ')',
    // );
    // }

    console.log('SafeArea insets top:', StaticSafeAreaInsets.safeAreaInsetsTop);
  }

  async openLink(urlStr) {
    try {
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.open(urlStr, {
          // iOS Properties
          dismissButtonStyle: 'close',
          preferredBarTintColor: '#ffffff',
          preferredControlTintColor: '#000000',
          readerMode: false,
          animated: true,
          modalPresentationStyle: 'fullScreen',
          modalTransitionStyle: 'crossDissolve',
          modalEnabled: true,
          enableBarCollapsing: false,
          // Android Properties
          showTitle: false,
          showInRecents: true,
          hasBackButton: true,
          toolbarColor: '#ffffff',
          secondaryToolbarColor: '#ffffff',
          navigationBarColor: '#000000', // below the device
          navigationBarDividerColor: '#ffffff', // a line color of navigation bar
          enableUrlBarHiding: true,
          enableDefaultShare: true,
          forceCloseOnRedirection: false,
          // includeReferrer: true,
          // Specify full animation resource identifier(package:anim/name)
          // or only resource name(in case of animation bundled with app).
          // animations: {
          //   startEnter: 'slide_in_bottom',
          //   startExit: 'slide_out_left',
          //   endEnter: 'slide_in_top',
          //   endExit: 'slide_out_right',
          // },
        });
        console.log(result);
        if (result.type === 'cancel') {
          if (Platform.OS == 'ios') {
            this.setState({
              allowApplePay: false,
            });
          }
          let incomingInappBrowserResult = JSON.stringify({
            closeTriggerType: result.type,
          });
          // console.log(
          //   'incomingIntentURLandroid--->',
          //   incomingInappBrowserResult,
          // );
          this.webView.ref.injectJavaScript(
            'window.handleIncomingIntentURL(' +
              incomingInappBrowserResult +
              ')',
          );
        } else {
          this.isInappBrowserClosed = result.type;
        }
      } else Linking.openURL(urlStr);
    } catch (error) {
      // Alert.alert(error.message);
      console.log(error.message);
    }
  }
  // Handle non http and https request
  handleNonHttpsRequest(url) {
    Linking.openURL(url);
  }
  reload() {
    this.setState({
      webviewKey: this.state.webviewKey + 1,
    });
  }
  handleObjectFromDevice = deviceInfo => {
    // You can now use the 'deviceInfo' object in your parent component
    this.storeDeviceInfo = deviceInfo;
  };
  // onCrashPress() {
  //   // crashlytics().crash();
  //   Sentry.nativeCrash();
  //   // var obj = { "html": "Hello" };
  //   // PrinterModule.printService(obj);
  // }
  checkBackgroundLocationEnabled() {
    LocationModule.isBackgroundLocationEnabled(allowAlwaysGranted => {
      console.log('isBackgroundLocationEnabled:', allowAlwaysGranted);
      this.setState({
        isBGLocationPermissionEnabled: allowAlwaysGranted,
      });
      console.log(
        'isBGLocationPermissionEnabled--> ',
        this.state.shoulShowBgpopup,
      );
      if (this.storeDeviceInfo.deviceOSversion >= 10) {
        if (!this.state.isBGLocationPermissionEnabled) {
          if (this.webView.ref) {
            this.webView.ref.injectJavaScript(
              `window.showBackgroundLocationPermissionAlert()`,
            );
          }
        } else {
          console.log('allowAlwaysGranted: ', allowAlwaysGranted);
        }
      }
    });
  }
  checkBatteryOptimizationEnabled() {
    LocationModule.isBatteryOptimizationEnabled(isEnabled => {
      // alert(isEnabled);
      this.setState({
        isBatteryRestiction: isEnabled,
      });
      if (!this.state.isBatteryRestiction) {
        if (this.webView.ref) {
          this.webView.ref.injectJavaScript(
            `window.showDisableBatteryOptimization()`,
          );
        }
      } else {
        console.log('DoNotshowBatteryPop', this.state.isBatteryRestiction);
        // alert('DoNotshowBatteryPop-up');
      }
    });
  }

  // This method will invoke when user denied the physical activity permission
  checkPhysicalActivityEnabled() {
    LocationModule.isActivityPermissionEnabled(isPhysicalActivityEnabled => {
      if (!isPhysicalActivityEnabled) {
        // console.log(
        //   'isActivityPermissionDenied----> ',
        //   isPhysicalActivityEnabled,
        // );
        if (this.webView.ref) {
          this.webView.ref.injectJavaScript(
            `window.showPhysicalActivityPermissionAlert()`,
          );
        }
      } else {
        // console.log(
        //   'isActivityPermissionEnabled----> ',
        //   isPhysicalActivityEnabled,
        // );
      }
    });
  }
  handleObjectFromDevice = deviceInfo => {
    // You can now use the 'deviceInfo' object in your parent component
    // console.log('Received object from Device:', deviceInfo);
    this.storeDeviceInfo = deviceInfo;
    console.log('storeDeviceInfo---> ', this.storeDeviceInfo);
  };

  render() {
    // const windowWidth = Dimensions.get('window').width;
    // const windowHeight = Dimensions.get('window').height;
    console.log('render--->', this.state.bgColor);
    const INJECTED_JAVASCRIPT = `(function() {
      const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'); meta.setAttribute('name', 'viewport');document.updateDeviceTokenForPushNative("Token"); document.getElementsByTagName('head')[0].appendChild(meta);
    })();`;
    return (
      <View style={{flex: 1}}>
        <DeviceDetails onSendObjectToParent={this.handleObjectFromDevice} />
        <AppShare ref={this.appShareRef} />
        <View
          style={{
            width: '100%',
            height: this.statusBarHeight,
            // height: getStatusBarHeight(true),
            backgroundColor: this.state.statusbarColor,
          }}>
          <StatusBar
            backgroundColor={this.state.statusbarColor}
            barStyle={
              this.state.statusContent == 'dark'
                ? 'dark-content'
                : 'light-content'
            } //default == white && dark-content == black
          />
        </View>
        {/* <View style={{ flex: 1 }}> */}
        {/* // <SafeAreaView style={{flex: 1}} forceInset={{ bottom: 'never',top: 'never'}}> */}
        {/* <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} /> */}
        <KeyboardAvoidingView
          style={{flex: 1}}
          // behavior="height"
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          enabled={Platform.OS === 'android'}
          keyboardVerticalOffset={25}>
          {/* <Button title="CRASH" onPress={this.onCrashPress} /> */}
          <WebView
            key={this.state.webviewKey}
            bounces={false}
            overScrollMode="never"
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            originWhitelist={['*']}
            onContentProcessDidTerminate={event => {
              // this.webView.ref.reload();
              // this.callErrorAPI(event);
              //when webview getting killed next time start from splash
              // this.reload();
              // this.setState({
              //   load: true,
              // });
              RNRestart.Restart();
            }}
            allowFileAccess={true}
            useWebKit={true}
            allowsInlineMediaPlayback={true}
            // pullToRefreshEnabled={true}
            javaScriptCanOpenWindowsAutomatically={true}
            mixedContentMode="always"
            enableApplePay={this.state.allowApplePay}
            mediaCapturePermissionGrantType="grantIfSameHostElsePrompt"
            // geolocationEnabled={true}
            cacheEnabled={true}
            domStorageEnabled={true}
            ref={webView => {
              this.webView.ref = webView;
            }}
            source={{uri: this.state.appUrl}}
            style={{flex: 1}}
            injectedJavaScript={INJECTED_JAVASCRIPT}
            // onLoadStart={this.onLoadStartChangeView}
            onLoadEnd={this.onLoadEndChangeView}
            onError={this.handleError}
            renderError={() => <View></View>}
            setSupportMultipleWindows={false}
            onNavigationStateChange={this.onNavigationStateChange.bind(this)}
            onShouldStartLoadWithRequest={event => {
              console.log('Type', event.navigationType);
              // setTimeout(() => {
              //   this.webView.ref.injectJavaScript(
              //     'window.setNativeStatusBarHeight(' +
              //       StaticSafeAreaInsets.safeAreaInsetsTop +
              //       ')',
              //   );
              // }, 1000);
              // if (event.navigationType == 'undefined' || event.navigationType == undefined || event.navigationType == "click" || event.target == '_blank') {
              console.log('eventURRL::', event.navigationType);

              if (
                // event.navigationType == 'undefined' ||
                // event.navigationType == 'click' ||
                // event.navigationType == undefined ||
                event.target == '_blank' ||
                event.url.startsWith('tel:')
              ) {
                if (
                  !event.url.includes(this.state.appUrl) &&
                  (!event.url.startsWith('http') ||
                    !event.url.startsWith('https'))
                ) {
                  this.handleNonHttpsRequest(event.url);
                  return false;
                }
                this.openLink(event.url);
                return false;
              }
              // if (event.url.includes(this.state.appUrl)) {
              //   this.setState({
              //     shouldshowLoader: true,
              //   });
              // }
              return true;
            }}
            onMessage={event => {
              let data = event.nativeEvent.data;
              data = JSON.parse(data);
              console.log('post message', data);
              console.log('post message type', data.postMessageType);

              if (data == null) {
                return;
              }

              if (data.postMessageType == 'deviceTokenRequested') {
                this.getToken();
              } else if (data.postMessageType == 'GetNativeCurrentLocation') {
                console.log(
                  'GetNativeCurrentLocation send current location now',
                );
                this.getDeviceCurrentLocation();
              } else if (data.postMessageType == 'nativeVibrateShort') {
                console.log('nativeVibrateShort');
                Vibration.vibrate(150);
              } else if (data.postMessageType == 'nativeVibrateLong') {
                console.log('nativeVibrateLong');
                Vibration.vibrate(900);
              } else if (data.postMessageType == 'RequestPushCallback') {
                console.log('RequestPushCallback');
                if (this.state.notificationData.length > 0) {
                  this.deeplinkNotification(this.state.notificationData);
                  this.setState({
                    notificationData: '',
                  });
                }
              } else if (data.postMessageType == 'nativeAssets') {
                console.log('native assets ' + data);
                this.getSplashUpdateTimeFromStorage(data);
              } else if (data.postMessageType == 'openNativeExternalWebview') {
                this.openLink(data.url);
              } else if (data.postMessageType == 'requestAppToOpenShare') {
                // console.log('requestAppToOpenShare', data.dataForShare);
                this.appShareRef.current.openNativeShare(data.dataForShare);
              } else if (data.postMessageType == 'appLoaded') {
                console.log('appLoadedRecievedPostmessageRunSecond-> ', data);
                if (this.webView.ref) {
                  this.webView.ref.injectJavaScript(
                    'window.getDeviceInfo(' +
                      JSON.stringify(this.storeDeviceInfo) +
                      ')',
                  );
                }
                // if (Platform.OS == 'android') {
                //   this.getDeviceInformation();
                // }
                if (this.webView.ref) {
                  this.webView.ref.injectJavaScript(
                    `window.useNativeExternalBrowser=true;`,
                  );
                }
                // this.isAppLoaded = true;+
              } else if (data.postMessageType == 'driverAppLoaded') {
                console.log('driverAppLoaded-> ', data);
                if (Platform.OS == 'android') {
                  if (this.isDriverLoggedIn == true) {
                    this.checkLocationPermissionAndroid();
                    LocationModule.checkLocationStatus();
                    this.checkBatteryOptimizationEnabled();
                    this.checkPhysicalActivityEnabled();
                  }
                }

                if (Platform.OS == 'ios') {
                  this.getDeviceCurrentLocationNew();
                }
                // this.isAppLoaded = true;
              } else if (data.postMessageType == 'showAppLocationSetting') {
                LocationModule.showAppLocationPermissionSetting();
              } else if (
                data.postMessageType == 'openBatteryOptimizationSetting'
              ) {
                LocationModule.showAppLocationPermissionSetting();
              } else if (
                data.postMessageType == 'openPhyicalActivityPermissionSetting'
              ) {
                LocationModule.showAppLocationPermissionSetting();
              } else if (
                data.postMessageType == 'showAppLocationPermissionSetting'
              ) {
                if (Platform.OS == 'android') {
                  LocationModule.showAppLocationPermissionSetting();
                }
                if (Platform.OS == 'ios') {
                  Linking.openURL('app-settings:');
                }

                console.log(
                  'LocationModule >>> showAppLocationPermissionSetting',
                );

                // this.isLocationPermissionDialogShowwed = false;
              } else if (data.postMessageType == 'showBgLocationPermission') {
                if (Platform.OS == 'android') {
                  LocationModule.showAppLocationPermissionSetting();
                }
                if (Platform.OS == 'ios') {
                  Linking.openURL('app-settings:');
                }

                console.log(
                  'LocationModule >>> showAppLocationPermissionSetting',
                );

                // this.isLocationPermissionDialogShowwed = false;
              } else if (data.postMessageType == 'PrintOrderOnThermalPrinter') {
                /*************Merchant only********/
                console.log('PrintOrderOnThermalPrinter-> ', data.data);
                if (Platform.OS === 'android') {
                  PrinterModule.printService(data.data);
                }
              } else if (data.postMessageType == 'OpenUrl') {
                /*************Driver only********/
                //driver Navigation uses this to open external window browser
                //"http://maps.google.com/?saddr=My+Location&daddr=26.846758023463,80.946276653213"
                console.log('driverAppUrl-> ', data.url);
                Linking.openURL(data.url);
              } else if (data.postMessageType == 'AgentOnDuty') {
                //This injection for driver duty ON and calling an api

                console.log('Agent Duty ON Data-> ', data);
                // if (Platform.OS === 'ios') {
                console.log('platform chk');
                // LocationModule.showAlert(driverBootSettings,driverDetailPayload);//startService();
                LocationModule.startService(
                  this.driverBootSettings,
                  this.driverDetailPayload,
                );
                // }
              } else if (data.postMessageType == 'AgentNotOnDuty') {
                //This injection for driver duty OFF and calling an api
                console.log('Agent Duty OFF Data-> ', data);
                // if (Platform.OS === 'ios') {
                LocationModule.stopService(
                  this.driverBootSettings,
                  this.driverDetailPayload,
                );
                // }
              } else if (data.postMessageType == 'DriverAppBootSettings') {
                //This api post for calling location api's configuration here backend is posting location post api details
                console.log('DriverAppBootSettings -> ', data);
                this.driverBootSettings = data;
              } else if (data.postMessageType == 'AgentLoggedIn') {
                //This will give you team_id,agent_id & token for calling location update api
                console.log('AgentLoggedIn -> ', data);
                this.isDriverLoggedIn = true;
                // inject device details
                if (this.webView.ref) {
                  this.webView.ref.injectJavaScript(
                    'window.getDeviceInfo(' +
                      JSON.stringify(this.storeDeviceInfo) +
                      ')',
                  );
                }
                // check location permissions
                if (Platform.OS == 'android') {
                  this.checkLocationPermissionAndroid();
                  LocationModule.checkLocationStatus();
                  this.checkBatteryOptimizationEnabled();
                  this.checkPhysicalActivityEnabled();
                }
                this.driverDetailPayload = data;
              } else if (data.postMessageType == 'OpenExternalApplication') {
                // alert('callTheDriver')
                //This will allow to phone call for driver agent
                console.log('OpenExternalApplication -> ', data.payload);
                console.log('payloaddata', data.payload.data);
                Linking.openURL(data.payload.data);
                // this.OpenTelephoneDialerPayload = data
              } else if (data.postMessageType == 'StopNotificationSound') {
                console.log('StopNotificationSound -> ', data);
                if (Platform.OS === 'android') {
                  LocationModule.stopSound();
                }
              } else {
                /********************************/
                console.log('default else of postmessage', data);
                // console.log('updatead splash screen',Date.parse(updatedAtTime));
                // console.log('updatead splash screen',Math.floor(Date.now()));
              }
            }}
            onHttpError={syntheticEvent => {
              const {nativeEvent} = syntheticEvent;
              console.log('HTTP ERROR');
              console.warn(
                'WebView received error status code: ',
                nativeEvent.statusCode,
              );
              this.setState({
                webViewError: true,
                errorDescription: this.getErrorDescription(
                  nativeEvent.statusCode,
                ),
              });
            }}
          />
        </KeyboardAvoidingView>

        {this.generateModel()}
      </View>
    );
  }

  reloadApp() {
    RNRestart.Restart();
  }

  generateModel() {
    if (this.state.load) {
      return (
        <Modal
          animationType="fade"
          transparent={false}
          visible={this.state.load}
          // onRequestClose={() => {
          //   setModalVisible(!modalVisible);
          // }}
        >
          {this.renderLottie()}
        </Modal>
      );
    } else if (this.state.webViewError) {
      return (
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.webViewError}
          // onRequestClose={() => {
          //   setModalVisible(!modalVisible);
          // }}
        >
          {this.renderWebErrorPage()}
        </Modal>
      );
    }
  }

  renderLottie() {
    if (this.state.existImage) {
      console.log('render from image ' + this.state.fileImage);
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: width,
            height: height,
            //added by azeem
            backgroundColor: this.state.bgColor,
          }}>
          <Image
            source={{uri: 'file://' + this.state.fileImage}}
            style={{
              width: width,
              height: width,
            }}
          />
        </View>
      );
    } else if (this.state.existsJson) {
      console.log('render from json', this.state.existsJson);
      return (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: this.state.bgColor,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <LottieView
            style={{
              // flex: 1,
              // height: height,
              width: width,
              // aspectRatio: 1,
            }}
            ref={anim => (this.splashAnimation.ref = anim)}
            resizeMode="cover"
            source={this.state.fileJson}
            //  source={require('./animation.json')}
            autoPlay
            loop={true}

            //  onAnimationFinish={() =>
            // this.props.navigation.replace('Home')
            // }
          />
        </View>
      );
    } else if (this.state.existsMp4Video) {
      // console.log('render from mp4_video', this.state.existsMp4Video);
      // console.log('mp4file', this.state.fileMp4Video);
      return (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: this.state.bgColor,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Video
            // source={require('./splashVideo.mp4')}
            source={{uri: this.state.fileMp4Video}}
            style={{
              position: 'absolute',
              width: width,
              height: height,
              top: 0,
              left: 0,
            }}
            resizeMode="cover"
            repeat={true}
            muted={false}
            onEnd={() => console.log('Video ended')}
          />
        </View>
      );
    } else if (this.state.showLocalAnimation) {
      console.log('render from local');
      return (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: this.state.bgColor,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <LottieView
            style={{
              // flex: 1,
              // height: Dimensions.get('window').height,
              // width: Dimensions.get('window').width,
              padding: 0,
              margin: 0,
              width: width,
              aspectRatio: 1,
              backgroundColor: this.state.bgColor,
            }}
            ref={anim => (this.splashAnimation.ref = anim)}
            resizeMode="cover"
            //source={this.state.fileJson}
            source={require('./animation.json')}
            autoPlay
            loop={true}
            //  onAnimationFinish={() =>
            // this.props.navigation.replace('Home')
            // }
          />
        </View>
      );
    } else if (this.state.showLocalImage) {
      console.log('render local image');
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: width,
            height: height,
            backgroundColor: this.state.bgColor,
          }}>
          <Image
            source={require('./splash-icon.png')}
            style={{
              width: width,
              height: width,
            }}
          />
        </View>
      );
    } else if (this.state.showLocalMp4Video) {
      return (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: this.state.bgColor,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Video
            source={require('./splashVideo.mp4')}
            style={{
              position: 'absolute',
              width: width,
              height: height,
              top: 0,
              left: 0,
            }}
            resizeMode="cover"
            repeat={true}
            muted={false}
            onEnd={() => console.log('Video ended')}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: this.state.bgColor,
          }}
        />
      );
    }
  }
  renderWebErrorPage() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#161D23',
          paddingHorizontal: 20,
        }}>
        <SvgXml xml={errorXml} width="120" height="120" />

        {this.state.errorDescription != null && (
          <Text style={{fontSize: 20, paddingTop: 15, color: 'white'}}>
            {this.state.errorDescription}
          </Text>
        )}

        <Pressable
          style={{
            backgroundColor: '#0D1215',
            padding: 10,
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 60,
          }}
          onPress={() => {
            this.webView.ref.reload();
            this.setState({webViewError: false, load: true});
          }}>
          <SvgXml xml={refreshIcon} width="20" height="20" />
          <Text
            style={{
              color: 'white',
              fontSize: 18,
              fontWeight: 'bold',
              marginLeft: 10,
            }}>
            Retry
          </Text>
        </Pressable>
        <Text style={{color: 'white', textAlign: 'center'}}>
          {this.state.errorMessage}
        </Text>
      </View>
    );
  }
  componentDidMount() {
    console.log('APP STATE ' + this.app_state);

    // First, it tries to get the initial URL that the app was launched with.
    Linking.getInitialURL()
      .then(url => {
        console.log('launch url 1', url);
        if (url) {
          // If there is a valid URL, call the _handleOpenUrl function to handle it.
          this._handleOpenUrl({url});
        }
      })
      .catch(err => console.error('launch url error', err));
    // this.subscription = DeviceEventEmitter.addListener(
    //   'CloseBrowser', // Replace with the actual event name
    //   this.handleHyperzodEventIntent
    // );
    Linking.addEventListener('url', this.handleHyperzodEventIntent);
    if (this.app_state === undefined) {
      this.app_state = AppState.addEventListener(
        'change',
        this.onAppStateChange,
      );
      console.log('APP STATE >> ' + this.app_state);
    }
    if (Platform.OS === 'android') {
      BackHandler.addEventListener(
        'hardwareBackPress',
        this.onAndroidBackPress,
      );
    }

    eventEmitter.addListener('locationStatus', this.handleAppEvents);
    eventEmitter.addListener(
      'PermissionSettings',
      this.handleLocationPermissionEvents,
    );
    // this.callAPI();
    // this.onLoadEndChangeView();
    //  this.pushNotificationListner();
    // if (this.webView.ref) {
    //   this.webView.ref.injectJavaScript(
    //     `document.body.style.userselect='none'`,
    //   );
    // }
  }

  handleHyperzodEventIntent = UrlSchemeEvent => {
    // Log the incoming URL for debugging purposes
    console.log('incomingIntentURL---> ', UrlSchemeEvent);

    // Store the incoming event URL in a variable
    const incomingEventUrl = UrlSchemeEvent.url;
    // console.log('clientAppSchemeName-->', this.state.clientAppSchemeName);
    // Check if the incoming URL starts with specific schemes
    hyperzodInternalCustomUrl = incomingEventUrl.startsWith(
      'hyperzod://closeinappbrowser',
    );
    hyperzodClientCustomUrl = incomingEventUrl.startsWith(
      `${this.state.clientAppSchemeName}://app`,
    );

    // Create a JSON string with incoming event information
    let hyperzodIncomingIntentUrl = JSON.stringify({
      incomingCallbackIntenURL: incomingEventUrl,
      closeTriggerType: this.isInappBrowserClosed,
    });

    // Check the platform (iOS or Android)
    if (hyperzodInternalCustomUrl || hyperzodClientCustomUrl) {
      if (Platform.OS == 'ios') {
        // Disable Apple Pay if on iOS
        this.setState({
          allowApplePay: false,
        });

        // Close the in-app browser
        if (hyperzodInternalCustomUrl) {
          InAppBrowser.close();
        }
        // Inject JavaScript code into the WebView to handle the incoming intent URL
        this.webView.ref.injectJavaScript(
          'window.handleIncomingIntentURL(' + hyperzodIncomingIntentUrl + ')',
        );

        // Close the in-app browser
      } else {
        // For Android, log the hyperzodIncomingIntentUrl
        console.log('hello', hyperzodIncomingIntentUrl);

        // Close the in-app browser
        if (hyperzodInternalCustomUrl) {
          InAppBrowser.close();
        }
        // Inject JavaScript code into the WebView to handle the incoming intent URL
        this.webView.ref.injectJavaScript(
          'window.handleIncomingIntentURL(' + hyperzodIncomingIntentUrl + ')',
        );
      }
    }
  };

  // This is the _handleOpenUrl function that is called with the URL when it is available.
  _handleOpenUrl = initialUrlSchemeEvent => {
    console.log('handleOpenUrl', initialUrlSchemeEvent.url);
    let hyperzodIncomingIntentUrl = JSON.stringify({
      incomingCallbackIntenURL: initialUrlSchemeEvent.url,
    });
    // It is using the 'window.handleIncomingIntentURL' function and passing the event URL as an argument.
    this.webView.ref.injectJavaScript(
      'window.handleIncomingIntentURL(' + hyperzodIncomingIntentUrl + ')',
    );
  };

  onRegister(token) {
    console.log('Register token ', token);
  }

  onNotif(notification) {
    // Alert.alert(notification.title, notification.message);
    if (notification.foreground && !notification.userInteraction) {
      console.log('FOREGROUND:', notification);
      PushNotification.localNotification({
        channelId: '1',
        title: notification.title,
        message: notification.message,
        userInfo: notification.data,
      });
    }
    if (notification.foreground && notification.userInteraction) {
      var data =
        JSON.stringify(notification.data).length == 2
          ? 'No data'
          : JSON.stringify(notification.data);
      console.log('user clicked ', data);
      setTimeout(() => {
        this.webView.ref.injectJavaScript(
          `window.processNativePushNotification(${data})`,
        );
      }, 1000);
    }
  }

  async downloadFile(response, updatedSplashAt) {
    if (response == null) {
      return;
    }
    var url = response.app_splash_screen;
    console.log('Url is ', url);
    // var path = RNFS.DocumentDirectoryPath + '/react-native.png';
    // if (url.includes('.json')) {
    //   path = RNFS.DocumentDirectoryPath + '/react-native.json';
    // }
    var path;
    if (url.includes('.json')) {
      path = RNFS.DocumentDirectoryPath + '/react-native.json';
    } else if (url.includes('.mp4')) {
      path = RNFS.DocumentDirectoryPath + `/react-native.mp4`;
    } else {
      path = RNFS.DocumentDirectoryPath + '/react-native.png';
    }
    this.deleteFiles();
    RNFS.downloadFile({
      fromUrl: url,
      toFile: `${path}`,
    }).promise.then(async r => {
      console.log('downloaded ', r);
      console.log('path ', `${path}`);

      console.log('status bar', response.status_bar_bg_color);
      console.log('status content', response.status_bar_content_style);

      await AsyncStorage.setItem('configuration', JSON.stringify(response));

      if (updatedSplashAt != null) {
        AsyncStorage.setItem(
          'updatedSplashAT',
          JSON.stringify(updatedSplashAt),
        ).then(result => {});
      }
      console.log('SAVE time ' + updatedSplashAt);
    });
  }

  async requestPermissions() {
    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization();
      Geolocation.setRNConfiguration({
        skipPermissionRequests: false,
        authorizationLevel: 'always',
      });
    }

    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }
  }
  async getDeviceCurrentLocation() {
    if (Platform.OS === 'ios') {
      console.log('called');
      const status = await Geolocation.requestAuthorization('whenInUse');
      console.log('isStatusDenied->> ', this.state.isStatusDenied);
      console.log('status->> ', status);
      if (this.state.isStatusDenied === true && status !== 'granted') {
        this.showLocationErrorAlert();
      }
      if (status === 'granted') {
        Geolocation.getCurrentPosition(
          position => {
            let latLong =
              position.coords.latitude + ',' + position.coords.longitude;
            console.log('geolocation position is', latLong);
            if (this.webView.ref) {
              this.webView.ref.injectJavaScript(
                'window.setNativeCurrentLocation("' + latLong + '")',
              );
            }
          },
          error => {
            console.log('geolocation err is', error.code, error.message);
          },
          {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        );
      } else if (status === 'denied') {
        console.log('locationDenied->> ');
        this.setState({
          isStatusDenied: true,
        });
        // this.showLocationErrorAlert();
      } else if (status === 'disabled') {
        this.showLocationErrorAlert();
      } else {
        this.showLocationErrorAlert();
      }
    } else {
      // this will request location and notification permission
      const permissionsToRequest = [
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ];
      if (Platform.Version >= 33) {
        permissionsToRequest.push('android.permission.POST_NOTIFICATIONS');
      }
      const status = await PermissionsAndroid.requestMultiple(
        permissionsToRequest,
      );
      console.log(Platform.Version);
      console.log('Permission status:', status);
      if (
        status[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION] ===
        PermissionsAndroid.RESULTS.GRANTED
      ) {
        // Permission granted, proceed with geolocation
        Geolocation.getCurrentPosition(
          position => {
            let latLong =
              position.coords.latitude + ',' + position.coords.longitude;
            console.log('geolocation position is', latLong);

            // Assuming this is part of a React component, make sure to handle 'this' appropriately
            if (this.webView.ref) {
              this.webView.ref.injectJavaScript(
                'window.setNativeCurrentLocation("' + latLong + '")',
              );
            }
          },
          error => {
            console.log('geolocation err is', error.code, error.message);
          },
          {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        );
      } else if (
        status[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION] ===
        PermissionsAndroid.RESULTS.DENIED
      ) {
        // Permission denied
        console.log('Location permission denied');
        // Handle denied permission (e.g., show an alert)
      } else if (
        status[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION] ===
        PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN
      ) {
        // Permission permanently denied
        console.log('Location permission permanently denied');
        // Provide guidance on how to enable the permission in settings
        this.showLocationErrorAlert();
      } else {
        // Handle other cases (unlikely to occur in practice)
        console.log('Location permission not granted');
        this.showLocationErrorAlert();
      }
    }
  }

  showLocationErrorAlert() {
    Alert.alert(
      'Enable Location Service',
      'To see stores near you, please enable location services for this app in your device settings.',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Setting',
          onPress: () => {
            if (Platform.OS === 'ios') {
              Linking.openURL('app-settings:');
            } else {
              Linking.openSettings();
            }
          },
          style: 'cancel',
        },
      ],
      {
        cancelable: true,
        onDismiss: () =>
          Alert.alert(
            'This alert was dismissed by tapping outside of the alert dialog.',
          ),
      },
    );
  }

  async deleteFiles() {
    let exists = await RNFS.exists(
      RNFS.DocumentDirectoryPath + '/react-native.png',
    );
    if (exists) {
      // exists call delete
      await RNFS.unlink(RNFS.DocumentDirectoryPath + '/react-native.png');
      console.log('File Deleted');
      // RNFS.mkdir(DirectoryPath);
    } else {
      console.log('File Not Available');
    }
    let existsJson = await RNFS.exists(
      RNFS.DocumentDirectoryPath + '/react-native.json',
    );

    if (existsJson) {
      // exists call delete
      await RNFS.unlink(RNFS.DocumentDirectoryPath + '/react-native.json');
      console.log('File Deleted');
      // RNFS.mkdir(DirectoryPath);
    } else {
      console.log('File Not Available');
    }
    let existsMp4Video = await RNFS.exists(
      RNFS.DocumentDirectoryPath + '/react-native.mp4',
    );

    if (existsMp4Video) {
      // exists call delete
      await RNFS.unlink(RNFS.DocumentDirectoryPath + '/react-native.mp4');
      console.log('MP4 File Deleted');
      // RNFS.mkdir(DirectoryPath);
    } else {
      console.log('File Not Available');
    }
  }

  componentWillUnmount() {
    if (this.app_state !== null) {
      this.app_state?.remove();
      this.app_state = null;
    }
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress');
    }
    if (this.subscription) {
      this.subscription.remove(); // Clean up the subscription
    }
  }

  onAndroidBackPress = () => {
    if (this.webView.ref) {
      this.webView.ref.goBack();
      return true;
    }
    return false;
  };

  /*
    callErrorAPI(event) {
    // this.checkPermission();
    //  this.deleteFiles();
    console.log('calling api');
    var res = fetch(
      'http://skmobileapp.co.in/cyoe_v1/index.php/sendErrorLog?error=' +
      event.url.toString(),
      {
        method: 'GET',
      },
    );
    // let json = await res.json();
    // console.log(json);
  }
  */

  /*async checkPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'App needs access to memory to download the file ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      } else {
        // Alert.alert(
        //   "Permission Denied!",
        //   "You need to give storage permission to download the file"
        // );
      }
    } catch (err) {
      console.warn(err);
    }
  }*/

  /*async callAPI() {
    // this.checkPermission();
    //  this.deleteFiles();
    var res = await fetch(
      'https://api.hyperzod.dev/settings/public/mobile?tenant_id=1',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'x-api-key': 'pEM60tPXkrUuC7WBva6hVUV8EZYd3i',
        },
      },
    );
    let json = await res.json();
    console.log(json);
    this.downloadFile(
      json.data.mobile_splash_screen.url,
      json.data.mobile_splash_screen.type,
    );
  }*/
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

const refreshIcon = `
<svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 52.5 (67469) - http://www.bohemiancoding.com/sketch -->
    <title>refresh</title>
    <desc>Created with Sketch.</desc>
    <g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Two-Tone" transform="translate(-274.000000, -3480.000000)">
            <g id="Navigation" transform="translate(100.000000, 3378.000000)">
                <g id="Two-Tone-/-Navigation-/-refresh" transform="translate(170.000000, 98.000000)">
                    <g>
                        <polygon id="Path" points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M17.65,6.35 C16.2,4.9 14.21,4 12,4 C7.58,4 4.01,7.58 4.01,12 C4.01,16.42 7.58,20 12,20 C15.73,20 18.84,17.45 19.73,14 L17.65,14 C16.83,16.33 14.61,18 12,18 C8.69,18 6,15.31 6,12 C6,8.69 8.69,6 12,6 C13.66,6 15.14,6.69 16.22,7.78 L13,11 L20,11 L20,4 L17.65,6.35 Z" id="ðŸ”¹-Primary-Color" fill="#ffffff"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>`;

const errorXml = `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<path style="fill:#FF9F19;" d="M445.215,478.835H66.848c-23.67,0-45.079-12.118-57.25-32.418
c-12.182-20.3-12.802-44.883-1.663-65.781L198.889,65.387c12.182-20.17,33.548-32.222,57.142-32.222s44.959,12.052,57.152,32.245
l190.499,314.433c11.585,21.692,10.966,46.274-1.217,66.575C490.294,466.717,468.885,478.835,445.215,478.835z"/>
<path style="fill:#F28618;" d="M445.215,478.835c23.67,0,45.079-12.118,57.25-32.418c12.182-20.3,12.802-44.883,1.217-66.575
L313.184,65.409c-12.193-20.193-33.559-32.245-57.152-32.245v445.67H445.215z"/>
<path style="fill:#486475;" d="M256.031,345.294c-9.215,0-16.693-7.477-16.693-16.693V161.675c0-9.215,7.477-16.693,16.693-16.693
s16.693,7.477,16.693,16.693v166.926C272.724,337.817,265.247,345.294,256.031,345.294z"/>
<path style="fill:#3C4D5C;" d="M256.031,412.061c-9.21,0-16.693-7.498-16.693-16.693s7.483-16.693,16.693-16.693
c9.21,0,16.693,7.498,16.693,16.693S265.241,412.061,256.031,412.061z"/>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>`;
