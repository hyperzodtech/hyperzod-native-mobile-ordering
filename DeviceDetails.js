import React, {Component} from 'react';
import DeviceInfo from 'react-native-device-info';
import {View, Text} from 'react-native';
class DeviceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      injectDeviceInformation: {},
    };
  }
  async componentDidMount() {
    try {
      await this.sendObjectToParent();
    } catch (error) {
      console.error('Error fetching device information:', error);
    }
  }
  sendObjectToParent = async () => {
    this.state.injectDeviceInformation.deviceOSversion =
      DeviceInfo.getSystemVersion();
    this.state.injectDeviceInformation.appName =
      DeviceInfo.getApplicationName();
    this.state.injectDeviceInformation.brand = DeviceInfo.getBrand();
    this.state.injectDeviceInformation.buildNumber =
      DeviceInfo.getBuildNumber();
    this.state.injectDeviceInformation.bundleId = DeviceInfo.getBundleId();
    this.state.injectDeviceInformation.model = DeviceInfo.getModel();
    this.state.injectDeviceInformation.version = DeviceInfo.getVersion();
    this.state.injectDeviceInformation.hasNotch = DeviceInfo.hasNotch();
    this.state.injectDeviceInformation.hasDynamicIsland =
      DeviceInfo.hasDynamicIsland();
    await DeviceInfo.isLocationEnabled().then(enabled => {
      this.state.injectDeviceInformation.isLocationEnabled = enabled;
    });
    await DeviceInfo.getManufacturer().then(manufacturer => {
      this.state.injectDeviceInformation.manufacturerName = manufacturer;
    });
    await DeviceInfo.getMaxMemory().then(maxMemory => {
      let maxMemoryInMb = maxMemory / (1024 * 1024);
      this.state.injectDeviceInformation.maxMemoryInMb = maxMemoryInMb;
    });

    await DeviceInfo.getPowerState().then(state => {
      this.state.injectDeviceInformation.stateBattery = state;
    });

    await DeviceInfo.getTotalDiskCapacity().then(capacity => {
      this.state.injectDeviceInformation.totalDiskCapacity =
        capacity / (1024 * 1024);
    });

    await DeviceInfo.getTotalMemory().then(totalMemory => {
      this.state.injectDeviceInformation.totalMemory =
        totalMemory / (1024 * 1024);
    });

    await DeviceInfo.getUsedMemory().then(usedMemory => {
      this.state.injectDeviceInformation.getUsedMemory =
        usedMemory / (1024 * 1024);
    });

    await DeviceInfo.getUserAgent().then(userAgent => {
      this.state.injectDeviceInformation.getUserAgent = userAgent;
    });

    await DeviceInfo.isAirplaneMode().then(airplaneModeOn => {
      this.state.injectDeviceInformation.isAirplaneMode = airplaneModeOn;
    });

    await DeviceInfo.isLandscape().then(isLandscape => {
      this.state.injectDeviceInformation.isLandscape = isLandscape;
    });

    // console.log(
    //   'injectDeviceInformation--->',
    //   this.state.injectDeviceInformation,
    // );
    if (this.props.onSendObjectToParent) {
      this.props.onSendObjectToParent(this.state.injectDeviceInformation);
    }
  };
  render() {
    return <></>;
  }
}

export default DeviceDetails;
